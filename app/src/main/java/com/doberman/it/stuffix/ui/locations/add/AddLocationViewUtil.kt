package com.doberman.it.stuffix.ui.locations.add

object AddLocationViewUtil {
    @JvmStatic
    fun submissionAvailable(isFormValid: Boolean, state: AddLocationViewModel.State?): Boolean {
        return isFormValid && state != AddLocationViewModel.State.Adding
    }
}