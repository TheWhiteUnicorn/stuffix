package com.doberman.it.stuffix.ui.itemsSet.checklistDispatch

class ItemSubViewModel(
    val model: ItemsChecklistDispatchRepository.SetItem,
    val parent: ItemsCheckLIstDispatchViewModel
) {
    fun onClick(): Boolean = TODO()
    fun onSelectClick() = parent.onItemChanged(model.header.id)
}