package com.doberman.it.stuffix.ui.locations.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import com.doberman.it.stuffix.common.archutils.appViewModels
import com.doberman.it.stuffix.databinding.FragmentLocationViewBinding

class ViewLocationFragment : Fragment() {
    private val args: ViewLocationFragmentArgs by navArgs()
    private val viewModel: ViewLocationViewModel by appViewModels {
        ViewLocationViewModel(viewLocation(), args.locationId)
    }

    private lateinit var dataBinding: FragmentLocationViewBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dataBinding = FragmentLocationViewBinding.inflate(inflater, container, false)
        return dataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dataBinding.lifecycleOwner = viewLifecycleOwner
        dataBinding.viewModel = viewModel
    }
}