package com.doberman.it.stuffix.ui.locations.view

import com.doberman.it.stuffix.common.data.locations.LocationModel
import com.doberman.it.stuffix.common.data.locations.LocationsDao
import kotlinx.coroutines.flow.Flow

class ViewLocationRepositoryImpl(
    private val dao: LocationsDao
): ViewLocationRepository {
    override fun getLocation(locationId: Long): Flow<LocationModel> = dao.locationFlow(locationId)
}