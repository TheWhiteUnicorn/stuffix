package com.doberman.it.stuffix.ui.itemsSet.checklistDispatch

object SetItemViewUtil {
    @JvmStatic
    fun checked(modelId: Long, selectedIds: Set<ItemsCheckLIstDispatchViewModel.SelectedItemData>?): Boolean =
        selectedIds?.any { it.id == modelId } == true
}