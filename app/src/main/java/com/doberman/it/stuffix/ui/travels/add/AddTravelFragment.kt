package com.doberman.it.stuffix.ui.travels.add

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.fragment.app.Fragment
import androidx.lifecycle.observe
import androidx.navigation.fragment.findNavController
import com.doberman.it.stuffix.common.archutils.appViewModels
import com.doberman.it.stuffix.common.data.locations.LocationModel
import com.doberman.it.stuffix.databinding.FragmentTravelAddBinding
import com.doberman.it.stuffix.ui.utils.hideKeyboard
import com.doberman.it.stuffix.ui.utils.popups.dateAndTime.DateAndTimePicker
import com.doberman.it.stuffix.ui.utils.popups.ExceptionHelper
import java.util.*


class AddTravelFragment : Fragment(), AddTravelViewModel.Navigation {
    private val viewModel: AddTravelViewModel by appViewModels {
        AddTravelViewModel(addTravel())
    }

    private lateinit var dataBinding: FragmentTravelAddBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        dataBinding = FragmentTravelAddBinding.inflate(inflater, container, false)
        return dataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        dataBinding.lifecycleOwner = viewLifecycleOwner
        dataBinding.viewModel = viewModel

        viewModel.navigation.observe(viewLifecycleOwner) { navFunction ->
            navFunction?.invoke(this@AddTravelFragment)
        }

        viewModel.state.observe(viewLifecycleOwner) { state ->
            when (state) {
                is AddTravelViewModel.State.Error -> ExceptionHelper.showSnack(
                    view,
                    state.throwable
                )
            }
        }

        dataBinding.spinnerDispatchLocation.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) = Unit

                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    viewModel.onDispatchLocationSelected(position)
                    hideKeyboard(view, context)
                }
            }

        dataBinding.spinnerArriveLocation.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) = Unit

                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    viewModel.onDestinationLocationSelected(position)
                    hideKeyboard(view, context)
                }
            }

        viewModel.locations.observe(viewLifecycleOwner) {
            displayDispatchLocations(it)
            displayArriveLocation(it)
        }
    }


    private fun displayDispatchLocations(locations: List<LocationModel>?) {
        locations ?: return

        dataBinding.spinnerDispatchLocation.adapter = ArrayAdapter(
            requireContext(),
            android.R.layout.simple_list_item_1,
            locations.map { it.title }
        )

        locations.indexOf(viewModel.selectedDispatchLocation.value)
            .takeIf { it > -1 }
            ?.let { dataBinding.spinnerDispatchLocation.setSelection(it) }
    }

    private fun displayArriveLocation(locations: List<LocationModel>?) {
        locations ?: return

        dataBinding.spinnerArriveLocation.adapter = ArrayAdapter(
            requireContext(),
            android.R.layout.simple_list_item_1,
            locations.map { it.title }
        )

        locations.indexOf(viewModel.selectedDestinationLocation.value)
            .takeIf { it > -1 }
            ?.let { dataBinding.spinnerArriveLocation.setSelection(it) }
    }

    override fun navigateToTravelView() {
        hideKeyboard(view, context)
        val action = AddTravelFragmentDirections.actionAddTravelToTravels()
        findNavController().navigate(action)
    }

    override fun showDispatchDateTimePicker() {
        val originDateTime = Calendar.getInstance()
        viewModel.dispatchDateTime.value?.let { originDateTime.time = it }
        context?.let {
            DateAndTimePicker.showDateAndTimePicker(
                originDateTime,
                callback = { calendar ->
                    viewModel.onDispatchTimeSelected(calendar.time)
                    hideKeyboard(view, context)
                },
                context = it
            )
        }
    }

    override fun showArriveDateTimePicker() {
        val originDateTime = Calendar.getInstance()
        viewModel.arriveDateTime.value?.let { originDateTime.time = it }
        context?.let {
            DateAndTimePicker.showDateAndTimePicker(
                originDateTime,
                callback = { calendar ->
                    viewModel.onArriveTimeSelected(calendar.time)
                    hideKeyboard(view, context)
                },
                context = it
            )
        }
    }
}