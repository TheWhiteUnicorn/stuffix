package com.doberman.it.stuffix.common.data.itemCategories

interface ItemCategory {
    val id: Long
    val title: String
}