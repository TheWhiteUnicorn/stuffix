package com.doberman.it.stuffix.common.data.auth

import com.doberman.it.stuffix.common.Constants
import com.google.gson.annotations.SerializedName
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.POST

interface AuthApiService {
    @POST(Constants.API.LOGIN_URL)
    suspend fun basicLogin(@Body request: LoginRequest): Response<LoginResponse>

    @POST(Constants.API.REGISTER_URL)
    suspend fun register(@Body request: RegistrationRequest): Response<LoginResponse>

    @GET(Constants.API.USER_DATA_URL)
    suspend fun getUserData(@Header("Authorization") token: String): UserDataResponse

    data class LoginRequest(
        @SerializedName("email")
        var email: String,

        @SerializedName("password")
        var password: String
    )

    data class RegistrationRequest(
        @SerializedName("email")
        var email: String,
        @SerializedName("password")
        var password: String,
        @SerializedName("first_name")
        var firstName: String,
        @SerializedName("last_name")
        var lastName: String
    )

    data class LoginResponse(
        val status: String,
        val data: LoginResponseData?,
        val error: String?
    ) {
        data class LoginResponseData(
            val id: Long,
            @SerializedName("google_id")
            val googleId: Long,
            val email: String,
            @SerializedName("first_name")
            val firstName: String,
            @SerializedName("last_name")
            val lastName: String,
            val token: String
        )
    }

    data class UserDataResponse(
        val status: String,
        val data: UserDataResponseData
    ) {
        data class UserDataResponseData(
            @SerializedName("user_id")
            override val id: Long,
            override val googleId: String,
            override val email: String,
            @SerializedName("first_name")
            override val firstName: String,
            @SerializedName("last_name")
            override val lastName: String
        ) : UserProfile
    }
}