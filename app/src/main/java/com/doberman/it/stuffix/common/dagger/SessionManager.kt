/**
 * (c) 2020 Denis Erashov
 * This code is licensed under MIT license (see LICENSE.txt for details)
 */

package com.doberman.it.stuffix.common.dagger

import android.content.Context
import android.content.SharedPreferences
import androidx.core.content.edit
import com.doberman.it.stuffix.common.data.auth.UserProfile
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.channelFlow
import kotlinx.coroutines.flow.flowOn

/**
 * A class that handles shared preferences interaction
 *
 * @constructor
 * Accepts application context
 *
 * @param context
 */
class SessionManager(context: Context) {
    // A pointer to shared preferences interface
    private var prefs: SharedPreferences =
        context.getSharedPreferences(PREFS_IDENTIFIER, Context.MODE_PRIVATE)

    /**
     * Constants that are used in interaction with preferences
     */
    companion object {
        const val PREFS_IDENTIFIER = "stuffix_shared_prefs"

        const val USER_TOKEN = "user_token"
        const val USER_ID = "user_id"
        const val USER_EMAIL = "user_email"
        const val USER_FIRST_NAME = "user_first_name"
        const val USER_LAST_NAME = "user_last_name"
    }

    /**
     * Save auth token in preferences file
     *
     * @param token - value to save
     */
    fun saveAuthToken(token: String) {
        prefs.edit { putString(USER_TOKEN, token) }
    }

    /**
     * Get auth token form local preferences file
     *
     * @return fetched token or null if no data found
     */
    fun fetchAuthToken(): String? {
        return prefs.getString(USER_TOKEN, null)
    }

    /**
     * Save user profile data in preferences file
     *
     * @param profile profile data to save
     */
    fun setUserProfile(profile: UserProfileImpl) {
        prefs.edit {
            putLong(USER_ID, profile.id)
            putString(USER_EMAIL, profile.email)
            putString(USER_FIRST_NAME, profile.firstName)
            putString(USER_LAST_NAME, profile.lastName)
        }
    }

    /**
     * Get user profile data form local preferences file
     *
     * @return fetched user profile data or null if no data found
     */
    @Throws(UninitializedPreferencesValueAccessException::class)
    fun fetchUserProfile(_prefs: SharedPreferences = prefs): UserProfile? {
        val id = _prefs.getLong(USER_ID, -1).takeIf { it != -1L } ?: return null
        val email = _prefs.getString(USER_EMAIL, null)
            ?: throw UninitializedPreferencesValueAccessException()
        val firstName = _prefs.getString(USER_FIRST_NAME, null)
            ?: throw UninitializedPreferencesValueAccessException()
        val lastName = _prefs.getString(USER_LAST_NAME, null)
            ?: throw UninitializedPreferencesValueAccessException()

        return UserProfileImpl(
            id = id,
            email = email,
            firstName = firstName,
            lastName = lastName
        )
    }

    /**
     * Exposes a user profile data wrapped up in Flow
     *
     * @return user profile data wrapped up in Flow
     */
    @ExperimentalCoroutinesApi
    fun userProfileFlow(): Flow<UserProfile?> {
        val flow: Flow<UserProfile?> = channelFlow {
            offer(fetchUserProfile(prefs))

            val listener =
                SharedPreferences.OnSharedPreferenceChangeListener { sharedPreferences, key ->
                    if (key in arrayOf(
                            USER_ID,
                            USER_EMAIL,
                            USER_FIRST_NAME,
                            USER_LAST_NAME
                        )
                    ) {
                        val profile = fetchUserProfile(sharedPreferences)
                        offer(profile)
                    }
                }

            prefs.registerOnSharedPreferenceChangeListener(listener)
            awaitClose { prefs.unregisterOnSharedPreferenceChangeListener(listener) }
        }
        return flow.flowOn(Dispatchers.Default)
    }

    /**
     * Clear user token and all profile data from local preferences file
     */
    fun suspendAuthenticatedSession() {
        prefs.edit {
            clear()
        }
    }

    /**
     * А data class that holds data about user profile
     *
     * @property id an id of a user, the same as on the backend
     * @property googleId user id provided by google auth service
     * @property email
     * @property firstName
     * @property lastName
     */
    data class UserProfileImpl(
        override val id: Long,
        override val googleId: String? = null,
        override val email: String,
        override val firstName: String,
        override val lastName: String
    ) : UserProfile

    /**
     * An exception raised when some piece of user profile data is missing
     */
    class UninitializedPreferencesValueAccessException : RuntimeException()
}