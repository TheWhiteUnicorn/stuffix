package com.doberman.it.stuffix.ui.locations.list

import com.doberman.it.stuffix.common.data.locations.LocationModel
import com.doberman.it.stuffix.common.data.locations.LocationsDao
import kotlinx.coroutines.flow.Flow

class LocationsListRepositoryImpl(
    private val dao: LocationsDao
) : LocationsListRepository {
    override fun locationsFlow(): Flow<List<LocationModel>> = dao.locationsFlow()
    override suspend fun archiveLocations(locationIds: Set<Long>) = dao.archive(locationIds)
}