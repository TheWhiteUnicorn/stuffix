package com.doberman.it.stuffix.ui.travels.view.takenItemsSection

import androidx.lifecycle.ViewModel
import com.doberman.it.stuffix.ui.travels.view.ViewTravelRepository
import com.doberman.it.stuffix.ui.travels.view.ViewTravelViewModel

class SetItemSubViewModel(
    val model: ViewTravelRepository.SetItem,
    val parent: ViewTravelViewModel
) : ViewModel() {
    fun onClick() {
        TODO()
    }
}