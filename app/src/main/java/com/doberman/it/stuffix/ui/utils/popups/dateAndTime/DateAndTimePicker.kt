package com.doberman.it.stuffix.ui.utils.popups.dateAndTime

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Context
import java.util.*

object DateAndTimePicker {
    fun showDateAndTimePicker(
        date: Calendar,
        maxDate: Calendar? = null,
        minDate: Calendar? = null,
        callback: (Calendar) -> Unit,
        context: Context
    ) {
        selectDate(
            date,
            maxDate,
            minDate,
            { calendar ->
                selectTime(
                    calendar,
                    callback,
                    context
                )
            },
            context
        )
    }

    private fun selectDate(
        date: Calendar,
        maxDate: Calendar? = null,
        minDate: Calendar? = null,
        callback: (Calendar) -> Unit,
        context: Context
    ) {
        val initYear = date.get(Calendar.YEAR)
        val initMonth = date.get(Calendar.MONTH)
        val initDayOfMonth = date.get(Calendar.DAY_OF_MONTH)
        val initHourOfDay = date.get(Calendar.HOUR_OF_DAY)
        val initMinute = date.get(Calendar.MINUTE)

        val dialog = DatePickerDialog(
            context,
            { _, i, i2, i3 ->
                callback(Calendar.getInstance().apply { set(i, i2, i3, initHourOfDay, initMinute) })
            },
            initYear,
            initMonth,
            initDayOfMonth
        )
        maxDate?.let { dialog.datePicker.maxDate = it.timeInMillis}
        minDate?.let { dialog.datePicker.minDate = it.timeInMillis }
        dialog.show()
    }

    private fun selectTime(
        date: Calendar,
        callback: (Calendar) -> Unit,
        context: Context
    ) {
        val initYear = date.get(Calendar.YEAR)
        val initMonth = date.get(Calendar.MONTH)
        val initDayOfMonth = date.get(Calendar.DAY_OF_MONTH)
        val initHourOfDay = date.get(Calendar.HOUR_OF_DAY)
        val initMinute = date.get(Calendar.MINUTE)

        val dialog = TimePickerDialog(
            context,
            {
                _, h, m ->
                callback(Calendar.getInstance().apply { set(initYear, initMonth, initDayOfMonth, h, m) })
            },
            initHourOfDay,
            initMinute,
            true
        )
        dialog.show()
    }
}