package com.doberman.it.stuffix.ui.itemsSet.make

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.doberman.it.stuffix.ui.itemsSet.make.tabContent.ARG_FILTER_OPTION
import com.doberman.it.stuffix.ui.itemsSet.make.tabContent.FilterOptions
import com.doberman.it.stuffix.ui.itemsSet.make.tabContent.MakeItemsSetTabContentFragment

class ItemsSetStateAdapter(val fragment: ItemsSetFragment) : FragmentStateAdapter(fragment) {
    override fun getItemCount(): Int = 3

    override fun createFragment(position: Int): Fragment {
        return when (position) {
            0 -> {
                val contentFragment =
                    MakeItemsSetTabContentFragment(fragment.viewModel)
                contentFragment.arguments = Bundle().apply {
                    putSerializable(ARG_FILTER_OPTION, FilterOptions.ALL)
                }
                contentFragment
            }
            1 -> {
                val contentFragment =
                    MakeItemsSetTabContentFragment(fragment.viewModel)
                contentFragment.arguments = Bundle().apply {
                    putSerializable(ARG_FILTER_OPTION, FilterOptions.TAKEN)
                }
                contentFragment
            }
            2 -> {
                val contentFragment =
                    MakeItemsSetTabContentFragment(fragment.viewModel)
                contentFragment.arguments = Bundle().apply {
                    putSerializable(ARG_FILTER_OPTION, FilterOptions.LEFT)
                }
                contentFragment
            }
            else -> throw Exception("Unexpected position")
        }
    }
}