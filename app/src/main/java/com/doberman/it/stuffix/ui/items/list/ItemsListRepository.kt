package com.doberman.it.stuffix.ui.items.list

import com.doberman.it.stuffix.common.data.itemCategories.ItemCategory
import com.doberman.it.stuffix.common.data.items.ItemModel
import com.doberman.it.stuffix.common.data.items.ItemsDao
import com.doberman.it.stuffix.common.data.locations.LocationModel
import kotlinx.coroutines.flow.Flow

interface ItemsListRepository {
    fun getItemsFlow(): Flow<List<Item>>

    suspend fun archiveItems(itemsIds: Set<Long>)

    class Item(
        val header: ItemsDao.ItemModelImpl,
        val location: LocationModel,
        val category: ItemCategory
    )
}