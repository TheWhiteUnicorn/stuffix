package com.doberman.it.stuffix.ui.items.list

import com.doberman.it.stuffix.R
import com.doberman.it.stuffix.databinding.ItemTravelItemBinding
import com.doberman.it.stuffix.ui.utils.recycler.IDataBindingView

class ItemSelectableCell(
    private val viewModel: ItemSubViewModel
) : IDataBindingView<ItemTravelItemBinding>{
    override val layoutID: Int = R.layout.item_travel_item

    override fun bindTo(binding: ItemTravelItemBinding) {
        binding.viewModel = viewModel
    }

    override fun unbindFrom(binding: ItemTravelItemBinding) = Unit
}