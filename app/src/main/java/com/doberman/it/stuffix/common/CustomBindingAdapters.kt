package com.doberman.it.stuffix.common

import android.graphics.BitmapFactory
import android.net.Uri
import android.view.View
import android.widget.ImageView
import androidx.databinding.BindingAdapter

object CustomBindingAdapters {
    @JvmStatic
    @BindingAdapter("visible")
    fun showHide(view: View, value: Boolean) {
        view.visibility = if (value)
            View.VISIBLE
        else
            View.INVISIBLE
    }

    @JvmStatic
    @BindingAdapter("gone")
    fun gone(view: View, gone: Boolean) {
        view.visibility = if (gone)
            View.GONE
        else
            View.VISIBLE
    }

    @JvmStatic
    @BindingAdapter("srcFilePath")
    fun setImageResource(imageView: ImageView, filePath: String?) {
        imageView.setImageBitmap(BitmapFactory.decodeFile(filePath))
    }
}
