package com.doberman.it.stuffix.ui

import androidx.lifecycle.LiveData
import com.doberman.it.stuffix.common.data.auth.UserProfile

interface MainActivityRepository {
    fun getUserProfile(): LiveData<UserProfile>
    fun suspendAuthenticatedSession()
    fun actualizeSignedInAccount()
}