package com.doberman.it.stuffix.ui.auth.signUp

import android.content.Context
import com.doberman.it.stuffix.R
import com.doberman.it.stuffix.ui.utils.Validation

object SignUpViewUtil {
    @JvmStatic
    fun isLoading(state: SignUpViewModel.State): Boolean =
        state == SignUpViewModel.State.Processing

    @JvmStatic
    fun isError(state: SignUpViewModel.State): Boolean =
        state is SignUpViewModel.State.Error

    @JvmStatic
    fun getGeneralErrorText(state: SignUpViewModel.State, context: Context): String? =
        if (state is SignUpViewModel.State.Error) {
            if (state.throwable is SignUpViewModel.MissingFieldValueException) {
                when (state.throwable.field) {
                    SignUpViewModel.MissingFieldValueException.FieldType.Email -> context.getString(
                        R.string.err_missing_val_email
                    )
                    SignUpViewModel.MissingFieldValueException.FieldType.Password -> context.getString(
                        R.string.err_missing_val_password
                    )
                    SignUpViewModel.MissingFieldValueException.FieldType.GivenName -> context.getString(
                        R.string.err_missing_val_given_name
                    )
                    SignUpViewModel.MissingFieldValueException.FieldType.FamilyName -> context.getString(
                        R.string.err_missing_val_family_name
                    )
                }
            }
            state.throwable.message
        } else null

    @JvmStatic
    fun emailError(value: String?, context: Context): String? {
        val isValid = Validation.isFieldValueEmpty(value) || Validation.isEmailValid(value)
        return if (isValid) {
            null
        } else {
            context.getString(R.string.validation_msg_email)
        }
    }

    @JvmStatic
    fun passwordError(value: String?, context: Context): String? {
        val isValid = Validation.isFieldValueEmpty(value) || Validation.isPasswordValid(value)
        return if (isValid)
            null
        else
            context.getString(R.string.validation_msg_password)
    }

    @JvmStatic
    fun passwordRepeatError(value: String?, password1: String?, context: Context): String? {
        val isValid = Validation.isFieldValueEmpty(value)
                || value == password1
        return if (isValid)
            null
        else
            context.getString(R.string.validation_msg_repeated_password)
    }

    @JvmStatic
    fun givenNameError(value: String?, context: Context): String? {
        return null
    }

    @JvmStatic
    fun familyNameError(value: String?, context: Context): String? {
        return null
    }
}