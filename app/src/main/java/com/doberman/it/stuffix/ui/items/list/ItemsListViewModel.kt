package com.doberman.it.stuffix.ui.items.list

import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.doberman.it.stuffix.common.archutils.LiveProperty
import com.doberman.it.stuffix.common.archutils.SingleHandledEvent
import com.doberman.it.stuffix.ui.utils.selectableList.SelectionActionModeCallback
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch

class ItemsListViewModel(
    private val repository: ItemsListRepository
) : ViewModel(), SelectionActionModeCallback.Actions {
    val navigation = SingleHandledEvent<(Navigation) -> Unit>()

    val items = repository
        .getItemsFlow()
        .map { list -> list.map {ItemSubViewModel(it, this)} }
        .asLiveData(viewModelScope.coroutineContext)

    val selectedElements = LiveProperty<Set<Long>?>(null)
    internal var _selectedElements: Set<Long>? by selectedElements

    fun onFabPress() = viewModelScope.launch {
        navigation.value = { it.navigateToAddItem() }
    }

    fun itemLongChanged(modelId: Long) {
        _selectedElements = (_selectedElements ?: emptySet()) + modelId
    }

    fun onItemChanged(dataId: Long) {
        val selectedElements = _selectedElements ?: return
        _selectedElements = if (dataId in selectedElements)
            selectedElements - dataId
        else
            selectedElements + dataId
    }

    override fun cancelSelection() {
        _selectedElements = null
    }

    override fun archiveSelected() {
        viewModelScope.launch {
            repository.archiveItems(_selectedElements ?: return@launch)
            _selectedElements = null
        }
    }

    interface Navigation {
        fun navigateToAddItem()
    }
}