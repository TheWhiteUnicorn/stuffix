package com.doberman.it.stuffix.common

import android.app.Application
import androidx.room.Room
import com.doberman.it.stuffix.common.dagger.*

class Application : Application() {
    companion object {
        lateinit var repositories: RepositoryComponent
            private set
    }

    override fun onCreate() {
        super.onCreate()

        val room = Room
            .databaseBuilder(this, DaoProvider::class.java, "appDataBase")
            .fallbackToDestructiveMigration()
            .build()

        repositories = DaggerRepositoryComponent
            .builder()
            .daoProvider(room)
            .retrofitProvider(RetrofitProvider())
            .sessionManager(SessionManager(applicationContext))
            .gscProvider(GscProvider(applicationContext))
            .repositoriesProvider(RepositoriesProvider())
            .build()
    }
}