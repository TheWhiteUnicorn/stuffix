package com.doberman.it.stuffix.ui.itemsSet.make

import com.doberman.it.stuffix.common.data.items.ItemsDao
import com.doberman.it.stuffix.common.data.itemsSet.ItemsSetDao
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class ItemsSetRepositoryImpl(
    private val itemsDao: ItemsDao,
    private val itemsSetDao: ItemsSetDao
) : ItemsSetRepository {
    override fun getItemsFlow(): Flow<List<ItemsSetRepository.Item>> =
        itemsDao.itemsAllWithCategoryAndLocationFlow()
            .map { list ->
                list.map {
                    ItemsSetRepository.Item(
                        it.itemModel,
                        it.location,
                        it.category
                    )
                }
            }

    override suspend fun updateItemsSet(
        selectedItems: Set<ItemsSetViewModel.SelectedItemData>,
        itemsSetId: Long
    ) {
        val itemsSetItems = selectedItems.map {
            ItemsSetDao.ItemInItemsSetModelImpl(
                itemId = it.id,
                itemsSetId = itemsSetId,
                quantity = it.quantity
            )
        }
        itemsSetDao.deleteSetItemsForSet(itemsSetId)
        itemsSetDao.addItemSetItems(itemsSetItems)
    }
}