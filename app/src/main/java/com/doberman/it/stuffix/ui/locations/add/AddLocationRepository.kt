package com.doberman.it.stuffix.ui.locations.add

import com.doberman.it.stuffix.common.data.locations.LocationModel

/**
 * A repository for Add location screen
 *
 */
interface AddLocationRepository {
    suspend fun addLocation(location: LocationModel)
}