/**
 * (c) 2020 Denis Erashov
 * This code is licensed under MIT license (see LICENSE.txt for details)
 */

package com.doberman.it.stuffix.ui.locations.add

import com.doberman.it.stuffix.common.data.locations.LocationModel
import com.doberman.it.stuffix.common.data.locations.LocationsDao

/**
 * Implementation of a repository for Add location screen
 *
 * @property dao instance of Locations DAO
 */
class AddLocationRepositoryImpl(
    private val dao: LocationsDao
) : AddLocationRepository {
    /**
     * Add a new location
     *
     * @param location LocationModel class instance to add
     */
    override suspend fun addLocation(location: LocationModel) =
        dao.add(LocationsDao.LocationModelImpl(location))
}