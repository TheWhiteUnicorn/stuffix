/**
 * (c) 2020 Denis Erashov
 * This code is licensed under MIT license (see LICENSE.txt for details)
 */

package com.doberman.it.stuffix.ui.locations.list

import android.os.Bundle
import android.view.ActionMode
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.observe
import androidx.navigation.fragment.findNavController
import com.doberman.it.stuffix.common.archutils.appViewModels
import com.doberman.it.stuffix.databinding.FragmentLocationsListBinding
import com.doberman.it.stuffix.databinding.ItemLocationBinding
import com.doberman.it.stuffix.ui.utils.recycler.StaticRecyclerAdapter
import com.doberman.it.stuffix.ui.utils.selectableList.SelectionActionModeCallback
import kotlinx.android.synthetic.main.activity_main.*

/**
 * A fragment for locations list screen
 *
 */
class LocationsListFragment : Fragment(),
    LocationsListViewModel.Navigation {
    private val viewModel: LocationsListViewModel by appViewModels {
        LocationsListViewModel(
            locations()
        )
    }
    private lateinit var dataBinding: FragmentLocationsListBinding
    private lateinit var adapter: StaticRecyclerAdapter<ItemLocationBinding>
    private var actionMode: ActionMode? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dataBinding = FragmentLocationsListBinding.inflate(inflater, container, false)
        return dataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        dataBinding.lifecycleOwner = viewLifecycleOwner
        dataBinding.viewModel = viewModel

        adapter = StaticRecyclerAdapter(emptyList(), viewLifecycleOwner)
        dataBinding.locationsListRecyclerView.adapter = adapter

        actionMode = null

        viewModel.navigation.observe(viewLifecycleOwner) { it?.invoke(this) }

        viewModel.locations.observe(viewLifecycleOwner) {
            adapter.updateList(it.map(::LocationSelectableCell))
        }

        viewModel.selectedElements.observe(viewLifecycleOwner) { ids ->
            if (ids != null) {
                if(actionMode == null)
                    actionMode = activity?.toolbar?.startActionMode(SelectionActionModeCallback(viewModel))
            } else {
                actionMode?.finish()
                actionMode = null
            }

            actionMode?.title = "${ids?.size} Selected"
        }
    }

    override fun navigateToAddLocation() {
        val action = LocationsListFragmentDirections
            .actionNavigationLocationsToAddLocationFragment()
        findNavController().navigate(action)
    }

    override fun navigateToViewLocation(locationId: Long) {
        val action = LocationsListFragmentDirections.actionNavigationFragmentLocationsToNavigationFragmentViewLocation(locationId)
        findNavController().navigate(action)
    }
}
