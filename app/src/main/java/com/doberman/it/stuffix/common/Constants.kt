package com.doberman.it.stuffix.common

object Constants {
    object ActivityRequestCodes {
        const val RC_GOOGLE_SIGN_IN = 1
        const val RC_ITEM_IMAGE_PICK = 2
        const val RC_RW_PERMISSIONS = 3
    }

    object API {
        const val BASE_URL = "http://3f3e5f5c3135.ngrok.io/api/v1/"
        const val LOGIN_URL = "auth/signin"
        const val REGISTER_URL = "auth/signup"
        const val USER_DATA_URL = ""
    }
}