package com.doberman.it.stuffix.ui.itemsSet.make.tabContent

import com.doberman.it.stuffix.R
import com.doberman.it.stuffix.databinding.ItemSetItemForEditBinding
import com.doberman.it.stuffix.ui.utils.recycler.IDataBindingView

class SetItemSelectableCell(
    private val viewModel: SetItemSubViewModel
) : IDataBindingView<ItemSetItemForEditBinding>{
    override val layoutID: Int = R.layout.item_set_item_for_edit

    override fun bindTo(binding: ItemSetItemForEditBinding) {
        binding.viewModel = viewModel
    }

    override fun unbindFrom(binding: ItemSetItemForEditBinding) = Unit
}