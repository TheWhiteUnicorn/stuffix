package com.doberman.it.stuffix.common.data.itemsSet

import androidx.room.*
import com.doberman.it.stuffix.common.data.items.ItemsDao
import kotlinx.coroutines.flow.Flow
import java.util.*

@Dao
abstract class ItemsSetDao {
    @Transaction
    @Query("SELECT * FROM ItemsSetModelImpl WHERE id=:id")
    abstract fun getSetWithItems(id: Long): Flow<ItemsSetWithItems>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    abstract suspend fun add(itemsSet: ItemsSetModelImpl): Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun addItemSetItems(setItems: List<ItemInItemsSetModelImpl>)

    @Query("DELETE FROM ItemInItemsSetModelImpl WHERE itemsSetId = :setId")
    abstract suspend fun deleteSetItemsForSet(setId: Long)

    @Entity
    class ItemsSetModelImpl(
        @PrimaryKey(autoGenerate = true) override val id: Long,
        override val name: String? = null,
        override val creationDate: Date = Calendar.getInstance().time
    ) : ItemsSetModel

    @Entity(primaryKeys = ["itemId", "itemsSetId"])
    class ItemInItemsSetModelImpl(
        override val itemId: Long,
        override val itemsSetId: Long,
        override val quantity: Int
    ) : ItemInItemsSetModel

    class ItemsSetWithItems(
        @Embedded val itemsSet: ItemsSetModelImpl,
        @Relation(
            entity = ItemInItemsSetModelImpl::class,
            parentColumn = "id",
            entityColumn = "itemsSetId"
        )
        val itemsInSet: List<SetItemWithItemModel>
    )

    class SetItemWithItemModel(
        @Embedded val setItem: ItemInItemsSetModelImpl,
        @Relation(
            entity = ItemsDao.ItemModelImpl::class,
            parentColumn = "itemId",
            entityColumn = "id"
        )
        val itemWithCategoryAndLocation: ItemsDao.ItemWithCategoryAndLocation
    )
}