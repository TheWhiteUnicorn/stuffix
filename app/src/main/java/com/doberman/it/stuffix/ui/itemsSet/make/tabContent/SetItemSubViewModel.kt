package com.doberman.it.stuffix.ui.itemsSet.make.tabContent

import com.doberman.it.stuffix.ui.itemsSet.make.ItemsSetRepository
import com.doberman.it.stuffix.ui.itemsSet.make.ItemsSetViewModel

class SetItemSubViewModel(
    val model: ItemsSetRepository.Item,
    val parent: ItemsSetViewModel
) {
    fun checkIsSelected() = parent._selectedElements.any {it.id == model.header.id}

    fun onSelectClick() = parent.onItemPressed(model.header.id)
}
