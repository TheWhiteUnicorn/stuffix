package com.doberman.it.stuffix.ui.utils.popups.dateAndTime

import android.content.Context
import com.doberman.it.stuffix.R
import java.text.DateFormat
import java.util.*

object DateAndTimePickerSourceViewUtil {
    @JvmStatic
    fun getButtonText(date: Date?, context: Context): String {
        return date?.let { DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.SHORT).format(date) }
            ?: context.getString(R.string.hint_select_datetime)
    }
}