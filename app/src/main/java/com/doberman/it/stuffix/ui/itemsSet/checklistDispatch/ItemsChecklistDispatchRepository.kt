package com.doberman.it.stuffix.ui.itemsSet.checklistDispatch

import com.doberman.it.stuffix.common.data.itemCategories.ItemCategory
import com.doberman.it.stuffix.common.data.items.ItemsDao
import kotlinx.coroutines.flow.Flow

interface ItemsChecklistDispatchRepository {
    fun getSetItemsListFlow(travelId: Long): Flow<List<SetItem>>
    suspend fun createItemsTransactions(takenItems: Set<ItemsCheckLIstDispatchViewModel.SelectedItemData>, travelId: Long)
    suspend fun setTravelStatusActive(travelId: Long)

    class SetItem(
        val header: ItemsDao.ItemModelImpl,
        val category: ItemCategory,
        val quantity: Int
    )
}