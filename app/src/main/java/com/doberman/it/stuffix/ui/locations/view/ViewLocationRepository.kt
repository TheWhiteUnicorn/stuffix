package com.doberman.it.stuffix.ui.locations.view

import com.doberman.it.stuffix.common.data.locations.LocationModel
import kotlinx.coroutines.flow.Flow

interface ViewLocationRepository {
    fun getLocation(locationId: Long): Flow<LocationModel>
}