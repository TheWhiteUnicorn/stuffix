package com.doberman.it.stuffix.ui.travels.view

import com.doberman.it.stuffix.common.data.itemCategories.ItemCategory
import com.doberman.it.stuffix.common.data.items.ItemsDao
import com.doberman.it.stuffix.common.data.locations.LocationModel
import com.doberman.it.stuffix.common.data.travels.TravelsDao
import kotlinx.coroutines.flow.Flow

interface ViewTravelRepository {
    fun getTravelData(travelId: Long): Flow<Travel>
    fun getTakenItemsData(travelId: Long): Flow<List<SetItem>>
    suspend fun markTransactionsFinished(travelId: Long)
    suspend fun setTravelStatusFinished(travelId: Long)

    class Travel(
        val header: TravelsDao.TravelModelImpl,
        val startPoint: LocationModel,
        val destinationPoint: LocationModel
    )

    class SetItem(
        val header: ItemsDao.ItemModelImpl,
        val category: ItemCategory,
        val quantity: Int
    )
}