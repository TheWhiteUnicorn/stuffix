/**
 * (c) 2020 Denis Erashov
 * This code is licensed under MIT license (see LICENSE.txt for details)
 */

package com.doberman.it.stuffix.ui.locations.list

import com.doberman.it.stuffix.common.data.locations.LocationModel

/**
 * A sub view model that holds data and interaction callbacks for location list's cells
 *
 * @property model a data model of corresponding location
 * @property parent parent view model, to which some of interaction callbacks should be redirected
 */
class LocationSubViewModel(
    val model: LocationModel,
    val parent: LocationsListViewModel
) {
    fun onLongClick(): Boolean {
        if (parent._selectedElements != null)
            return false
        parent.itemLongChanged(model.id)
        return true
    }

    fun onSelectClick() = parent.onItemPressed( model.id)
}
