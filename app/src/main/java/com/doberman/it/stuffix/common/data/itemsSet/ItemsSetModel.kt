package com.doberman.it.stuffix.common.data.itemsSet

import java.util.*

interface ItemsSetModel {
    val id: Long
    val name: String?
    val creationDate: Date
}
