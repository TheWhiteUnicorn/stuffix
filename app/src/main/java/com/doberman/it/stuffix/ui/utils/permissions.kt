package com.doberman.it.stuffix.ui.utils

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import androidx.core.content.ContextCompat
import com.doberman.it.stuffix.common.Constants

fun checkStorageRwPermissions(context: Context, activity: Activity, onGranted: () -> Any) {
    when {
        (ContextCompat.checkSelfPermission(
            context,
            Manifest.permission.READ_EXTERNAL_STORAGE
        ) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(
                    context,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                ) == PackageManager.PERMISSION_GRANTED) -> {
            onGranted()
        }
        else -> {
            activity.requestPermissions(
                listOf(
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                ).toTypedArray(),
                Constants.ActivityRequestCodes.RC_RW_PERMISSIONS
            )
        }
    }
}