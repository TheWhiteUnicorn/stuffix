package com.doberman.it.stuffix.common.dagger

import com.doberman.it.stuffix.ui.MainActivityRepository
import com.doberman.it.stuffix.ui.auth.signIn.SignInRepository
import com.doberman.it.stuffix.ui.auth.signUp.SignUpRepository
import com.doberman.it.stuffix.ui.items.add.AddItemRepository
import com.doberman.it.stuffix.ui.items.list.ItemsListRepository
import com.doberman.it.stuffix.ui.itemsSet.checklistDispatch.ItemsChecklistDispatchRepository
import com.doberman.it.stuffix.ui.itemsSet.make.ItemsSetRepository
import com.doberman.it.stuffix.ui.locations.add.AddLocationRepository
import com.doberman.it.stuffix.ui.locations.list.LocationsListRepository
import com.doberman.it.stuffix.ui.locations.view.ViewLocationRepository
import com.doberman.it.stuffix.ui.splash.SplashRepository
import com.doberman.it.stuffix.ui.travels.add.AddTravelRepository
import com.doberman.it.stuffix.ui.travels.list.TravelsListRepository
import com.doberman.it.stuffix.ui.travels.view.ViewTravelRepository
import dagger.Component

@Component(
    dependencies = [
        DaoProvider::class,
        RetrofitProvider::class,
        SessionManager::class,
        GscProvider::class
    ],
    modules = [
        RepositoriesProvider::class
    ]
)
abstract class RepositoryComponent {
    abstract fun splash(): SplashRepository
    abstract fun mainActivity(): MainActivityRepository
    abstract fun locations(): LocationsListRepository
    abstract fun addLocation(): AddLocationRepository
    abstract fun viewLocation(): ViewLocationRepository
    abstract fun items(): ItemsListRepository
    abstract fun addItem(): AddItemRepository
    abstract fun travels(): TravelsListRepository
    abstract fun addTravel(): AddTravelRepository
    abstract fun viewTravel(): ViewTravelRepository
    abstract fun itemsSet(): ItemsSetRepository
    abstract fun itemsChecklistDispatch(): ItemsChecklistDispatchRepository
    abstract fun signIn(): SignInRepository
    abstract fun signUp(): SignUpRepository
}