package com.doberman.it.stuffix.ui.travels.list

import android.content.Context
import android.graphics.drawable.Drawable
import com.doberman.it.stuffix.R
import com.doberman.it.stuffix.common.data.travels.TravelModel

object TravelsListViewUtil {
    @JvmStatic
    fun formatStartAndDestination(travel: TravelsListRepository.Travel): String? {
        return "${travel.startPoint.title} - ${travel.destinationPoint.title}"
    }

    @JvmStatic
    fun isLoading(travels: List<TravelSubViewModel>?): Boolean =
        travels == null

    @JvmStatic
    fun isLoadComplete(travels: List<TravelSubViewModel>?): Boolean =
        travels != null

    @JvmStatic
    fun getStatusIcon(status: TravelModel.TravelStatus?, context: Context): Drawable? {
        status?.let {
            return when (it) {
                TravelModel.TravelStatus.PLANNED -> context.getDrawable(R.drawable.creation)
                TravelModel.TravelStatus.ACTIVE -> context.getDrawable(R.drawable.airballoon)
                TravelModel.TravelStatus.FINISHED -> context.getDrawable(R.drawable.flag_checkered)
                TravelModel.TravelStatus.CANCELED -> context.getDrawable(R.drawable.cancel)
            }
        } ?: return context.getDrawable(R.drawable.creation) // Can this crutch be fixed?
    }

    @JvmStatic
    fun getStatusIconColor(status: TravelModel.TravelStatus?, context: Context): Int? {
        status?.let {
            return when (it) {
                TravelModel.TravelStatus.PLANNED -> context.getColor(R.color.colorYellow)
                TravelModel.TravelStatus.ACTIVE -> context.getColor(R.color.colorGreen)
                TravelModel.TravelStatus.FINISHED -> context.getColor(R.color.colorDark)
                TravelModel.TravelStatus.CANCELED -> context.getColor(R.color.colorAccent)
            }
        } ?: return context.getColor(R.color.colorYellow)
    }
}