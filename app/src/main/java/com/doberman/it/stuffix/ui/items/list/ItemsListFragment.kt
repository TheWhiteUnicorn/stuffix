package com.doberman.it.stuffix.ui.items.list

import android.os.Bundle
import android.view.ActionMode
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.observe
import androidx.navigation.fragment.findNavController
import com.doberman.it.stuffix.common.archutils.appViewModels
import com.doberman.it.stuffix.databinding.FragmentTravelItemsListBinding
import com.doberman.it.stuffix.databinding.ItemTravelItemBinding
import com.doberman.it.stuffix.ui.utils.recycler.StaticRecyclerAdapter
import com.doberman.it.stuffix.ui.utils.selectableList.SelectionActionModeCallback
import kotlinx.android.synthetic.main.activity_main.*

class ItemsListFragment : Fragment(),
    ItemsListViewModel.Navigation {
    private val viewModel: ItemsListViewModel by appViewModels {
        ItemsListViewModel(items())
    }
    private lateinit var dataBinding: FragmentTravelItemsListBinding
    private lateinit var adapter: StaticRecyclerAdapter<ItemTravelItemBinding>
    private var actionMode: ActionMode? = null

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        dataBinding.lifecycleOwner = viewLifecycleOwner
        dataBinding.viewModel = viewModel

        viewModel.navigation.observe(viewLifecycleOwner) { it?.invoke(this) }

        adapter = StaticRecyclerAdapter(emptyList(), viewLifecycleOwner)
        dataBinding.itemsListRecyclerView.adapter = adapter

        actionMode = null

        viewModel.items.observe(viewLifecycleOwner, Observer {
            adapter.updateList(it.map(::ItemSelectableCell))
        })
        viewModel.selectedElements.observe(viewLifecycleOwner) { ids ->
            if (ids != null) {
                if(actionMode == null)
                    actionMode = activity?.toolbar?.startActionMode(SelectionActionModeCallback(viewModel))
            } else {
                actionMode?.finish()
                actionMode = null
            }

            actionMode?.title = "${ids?.size} Selected"
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dataBinding = FragmentTravelItemsListBinding.inflate(inflater, container, false)
        return dataBinding.root
    }

    override fun navigateToAddItem() {
        val action =
            ItemsListFragmentDirections.actionNavigationItemsToAddItemFragment()
        findNavController().navigate(action)
    }
}
