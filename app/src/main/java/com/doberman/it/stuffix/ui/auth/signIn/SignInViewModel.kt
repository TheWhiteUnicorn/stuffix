package com.doberman.it.stuffix.ui.auth.signIn

import android.content.ContentValues.TAG
import android.content.Context
import android.content.Intent
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.doberman.it.stuffix.R
import com.doberman.it.stuffix.common.archutils.LiveProperty
import com.doberman.it.stuffix.common.archutils.SingleHandledEvent
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import kotlinx.coroutines.launch


class SignInViewModel(
    private val repository: SignInRepository
) : ViewModel() {
    val navigation = SingleHandledEvent<(Navigation) -> Unit>()

    val state = LiveProperty<State>(State.Normal)
    private var _state: State by state

    val email = MutableLiveData<String>()
    val password = MutableLiveData<String>()

    fun onSignInSubmit(context: Context) = viewModelScope.launch {
        _state = State.LoggingIn
        try {
            repository.login(
                email.value ?: throw Exception(context.getString(R.string.err_missing_val_email)),
                password.value
                    ?: throw Exception(context.getString(R.string.err_missing_val_password))
            )
        } catch (t: Throwable) {
            _state = State.Error(t)
            return@launch
        }
        _state = State.Normal
        navigation.value = { it.navigateToHomeScreen() }
    }

    fun onGoogleSignIn() {
        navigation.value = {
            repository.getGsc()?.signInIntent?.let { intent ->
                it.startGoogleSignIn(intent)
            }
        }
    }


    fun handleSignInResult(completedTask: Task<GoogleSignInAccount>) {
        try {
            val account: GoogleSignInAccount? = completedTask.getResult(ApiException::class.java)

            repository.actualizeGsa()
            handleGoogleAccountExistence(account)
        } catch (e: ApiException) {

            Log.w(TAG, "signInResult:failed code=" + e.statusCode)
            handleGoogleAccountExistence(null)
        }
    }

    private fun handleGoogleAccountExistence(account: GoogleSignInAccount?) {
        if (account != null) {
            navigation.value = { it.navigateToHomeScreen() }
        }
    }

    fun handleNavigateToSignIn() {
        navigation.value = { it.navigateToSignIn() }
    }

    interface Navigation {
        fun startGoogleSignIn(intent: Intent)
        fun navigateToSignIn()
        fun navigateToHomeScreen()
    }

    sealed class State {
        object Normal : State()
        object LoggingIn : State()
        class Error(val throwable: Throwable) : State()
    }
}
