package com.doberman.it.stuffix.ui.splash

interface SplashRepository {
    suspend fun isEmpty(): Boolean
    suspend fun isAuthenticated(): Boolean

    suspend fun initCaches()
}