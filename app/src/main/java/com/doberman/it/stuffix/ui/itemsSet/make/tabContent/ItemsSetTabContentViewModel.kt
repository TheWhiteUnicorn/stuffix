package com.doberman.it.stuffix.ui.itemsSet.make.tabContent

import androidx.lifecycle.*
import com.doberman.it.stuffix.ui.itemsSet.make.ItemsSetRepository
import com.doberman.it.stuffix.ui.itemsSet.make.ItemsSetViewModel
import kotlinx.coroutines.flow.map

class ItemsSetTabContentViewModel(
    repository: ItemsSetRepository,
    private val parentViewModel: ItemsSetViewModel,
    private val filterOption: FilterOptions
) : ViewModel() {

    private val itemsAll = repository
        .getItemsFlow()
        .map { list -> list.map { SetItemSubViewModel(it, parentViewModel) } }
        .asLiveData(viewModelScope.coroutineContext)

    val items = MediatorLiveData<List<SetItemSubViewModel>>().apply {
        fun actualize(itemsAllChanged: Boolean = false) {
            when(filterOption) {
                // Items of All tab shouldn't fully update when something selected
                FilterOptions.ALL -> if(itemsAllChanged) this.value = itemsAll.value
                FilterOptions.TAKEN -> this.value = itemsAll.value?.filter { it.checkIsSelected() }
                FilterOptions.LEFT -> this.value = itemsAll.value?.filter { !it.checkIsSelected() }
            }
        }
        this.addSource(itemsAll) { actualize(true) }
        this.addSource(parentViewModel.selectedElements) { actualize() }
    }
}