package com.doberman.it.stuffix.ui.items.add

import com.doberman.it.stuffix.common.data.itemCategories.ItemCategory
import com.doberman.it.stuffix.common.data.items.ItemModel
import com.doberman.it.stuffix.common.data.locations.LocationModel
import kotlinx.coroutines.flow.Flow

interface AddItemRepository {
    fun locationsFlow(): Flow<List<LocationModel>>
    fun categoriesFlow(): Flow<List<ItemCategory>>
    suspend fun addTravelItem(item: ItemModel)
}
