package com.doberman.it.stuffix.ui.auth.signIn

import android.content.Context
import com.doberman.it.stuffix.R
import com.doberman.it.stuffix.ui.utils.Validation

object SignInViewUtil {
    @JvmStatic
    fun signInAvailable(
        email: String?,
        passwordText: String?,
        state: SignInViewModel.State
    ): Boolean = Validation.isEmailValid(email) && Validation.isPasswordValid(passwordText) && !isLoading(state)

    @JvmStatic
    fun isLoading(state: SignInViewModel.State): Boolean =
        state == SignInViewModel.State.LoggingIn

    @JvmStatic
    fun isError(state: SignInViewModel.State): Boolean =
        state is SignInViewModel.State.Error

    @JvmStatic
    fun getGeneralErrorText(state: SignInViewModel.State): String? =
        if (state is SignInViewModel.State.Error) {
            (state).throwable.message
        } else null

    @JvmStatic
    fun emailError(value: String?, context: Context): String? {
        val isValid = Validation.isFieldValueEmpty(value) || Validation.isEmailValid(value)
        return if (isValid) {
            null
        } else {
            context.getString(R.string.validation_msg_email)
        }
    }

    @JvmStatic
    fun passwordError(value: String?, context: Context): String? {
        val isValid = Validation.isFieldValueEmpty(value) || Validation.isPasswordValid(value)
        return if (isValid)
            null
        else
            context.getString(R.string.validation_msg_password)
    }
}