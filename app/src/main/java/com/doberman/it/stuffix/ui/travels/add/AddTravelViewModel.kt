package com.doberman.it.stuffix.ui.travels.add

import androidx.lifecycle.*
import com.doberman.it.stuffix.common.archutils.LiveProperty
import com.doberman.it.stuffix.common.archutils.SingleHandledEvent
import com.doberman.it.stuffix.common.data.locations.LocationModel
import kotlinx.coroutines.launch
import java.util.*

class AddTravelViewModel(
    private val repository: AddTravelRepository
) : ViewModel() {
    val navigation = SingleHandledEvent<(Navigation) -> Unit>()
    val state = LiveProperty<State>(State.Normal)
    private var _state: State by state

    val locations = repository
        .locationsFlow()
        .asLiveData(viewModelScope.coroutineContext)

    val title = MutableLiveData<String?>(null)

    var selectedDispatchLocation = LiveProperty<LocationModel?>(null)
    private var _selectedDispatchLocation: LocationModel? by selectedDispatchLocation

    var selectedDestinationLocation = LiveProperty<LocationModel?>(null)
    private var _selectedDestinationLocation: LocationModel? by selectedDestinationLocation

    val dispatchDateTime = LiveProperty<Date?>(null)
    private var _dispatchDateTime: Date? by dispatchDateTime

    val arriveDateTime = LiveProperty<Date?>(null)
    private var _arriveDateTime: Date? by arriveDateTime

    val note = MutableLiveData<String?>(null)

    val isValid = MediatorLiveData<Boolean>().apply {
        fun validate() {
            this.value = title.value?.isNotEmpty() == true &&
                    _selectedDispatchLocation != null &&
                    _selectedDestinationLocation != null
        }

        this.addSource(title) { validate() }
        this.addSource(selectedDispatchLocation) { validate() }
        this.addSource(selectedDestinationLocation) { validate() }
    }

    fun onDispatchLocationSelected(position: Int) {
        locations.value?.getOrNull(position)?.let { _selectedDispatchLocation = it }
    }

    fun onDestinationLocationSelected(position: Int) {
        locations.value?.getOrNull(position)?.let { _selectedDestinationLocation = it }
    }

    fun onDispatchTimeSelected(date: Date) {
        _dispatchDateTime = date
    }

    fun onArriveTimeSelected(date: Date) {
        _arriveDateTime = date
    }

    private fun onSelectDispatchTime() {
        navigation.value = { it.showDispatchDateTimePicker() }
    }

    val onSelectDispatchTimeCallback: () -> Unit = ::onSelectDispatchTime

    private fun onSelectArriveTime() {
        navigation.value = { it.showArriveDateTimePicker() }
    }

    val onSelectArriveTimeCallback: () -> Unit = ::onSelectArriveTime

    fun onSubmit() = viewModelScope.launch {
        _state = State.Adding
        try {
            if (_arriveDateTime?.let { _dispatchDateTime?.after(_arriveDateTime) } == true)
                throw Exception("Dispatch date can't be after arrive date")

            repository.addTravel(
                title = title.value?.takeIf { it.isNotEmpty() }
                    ?: throw Exception("Missing requited title value"),
                plannedDispatchDateTime = _dispatchDateTime,
                plannedArriveDateTime = _arriveDateTime,
                startPointId = _selectedDispatchLocation?.id
                    ?: throw Exception("Missing required dispatch location value"),
                destinationPointId = _selectedDestinationLocation?.id
                    ?: throw Exception("Missing required arrive location value"),
                note = note.value
            )
        } catch (t: Throwable) {
            _state = State.Error(t)
            return@launch
        }
        _state = State.Normal
        navigation.value = { it.navigateToTravelView() }
    }

    interface Navigation {
        fun navigateToTravelView()
        fun showDispatchDateTimePicker()
        fun showArriveDateTimePicker()
    }

    sealed class State {
        object Normal : State()
        object Adding : State()
        class Error(val throwable: Throwable) : State()
    }
}