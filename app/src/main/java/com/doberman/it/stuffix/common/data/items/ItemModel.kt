package com.doberman.it.stuffix.common.data.items

import android.net.Uri

interface ItemModel {
    val id: Long
    val title: String
    val description: String?
    val categoryId: Long?
    val locationId: Long?
    val photo: String?
    val archived: Boolean
}