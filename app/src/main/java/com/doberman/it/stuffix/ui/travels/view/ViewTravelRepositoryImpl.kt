package com.doberman.it.stuffix.ui.travels.view

import androidx.lifecycle.asLiveData
import com.doberman.it.stuffix.common.data.itemTransactions.ItemTransactionsDao
import com.doberman.it.stuffix.common.data.itemsSet.ItemsSetDao
import com.doberman.it.stuffix.common.data.travels.TravelModel
import com.doberman.it.stuffix.common.data.travels.TravelsDao
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.flow.*

class ViewTravelRepositoryImpl(
    private val travelsDao: TravelsDao,
    private val itemsSetDao: ItemsSetDao,
    private val transactionsDao: ItemTransactionsDao
) : ViewTravelRepository {
    override fun getTravelData(travelId: Long): Flow<ViewTravelRepository.Travel> {
        return travelsDao.getWithLocations(travelId)
            .map {
                ViewTravelRepository.Travel(
                    it.travelsModel,
                    it.startPoint,
                    it.destinationPoint
                )
            }
    }

    override fun getTakenItemsData(travelId: Long): Flow<List<ViewTravelRepository.SetItem>> {
        return flow {
            travelsDao.getTravelCleanFlow(travelId).collect { travel ->
                itemsSetDao.getSetWithItems(travel.stuffSetId).collect { itemsSet ->
                    emit(itemsSet.itemsInSet.map { setItemWithItem ->
                        ViewTravelRepository.SetItem(
                            header = setItemWithItem.itemWithCategoryAndLocation.itemModel,
                            category = setItemWithItem.itemWithCategoryAndLocation.category,
                            quantity = setItemWithItem.setItem.quantity
                        )
                    })
                }
            }
        }
    }

    override suspend fun markTransactionsFinished(travelId: Long) {
        transactionsDao.finishTransactions(travelId)
    }

    override suspend fun setTravelStatusFinished(travelId: Long) {
        travelsDao.updateStatus(travelId, TravelModel.TravelStatus.FINISHED)
    }
}