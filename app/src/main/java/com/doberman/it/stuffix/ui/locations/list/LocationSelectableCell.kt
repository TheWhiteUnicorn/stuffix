/**
 * (c) 2020 Denis Erashov
 * This code is licensed under MIT license (see LICENSE.txt for details)
 */

package com.doberman.it.stuffix.ui.locations.list

import com.doberman.it.stuffix.databinding.ItemLocationBinding
import com.doberman.it.stuffix.ui.utils.recycler.IDataBindingView

/**
 * A selectable location cell's databinding wrapper
 *
 * @property viewModel
 */
class LocationSelectableCell(
    private val viewModel: LocationSubViewModel
) : IDataBindingView<ItemLocationBinding> {
    override val layoutID: Int = com.doberman.it.stuffix.R.layout.item_location

    override fun bindTo(binding: ItemLocationBinding) {
        binding.viewModel = viewModel
    }

    override fun unbindFrom(binding: ItemLocationBinding) = Unit
}
