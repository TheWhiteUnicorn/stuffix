package com.doberman.it.stuffix.common.data.items

import android.net.Uri
import androidx.room.*
import com.doberman.it.stuffix.common.data.itemCategories.ItemCategoriesDao
import com.doberman.it.stuffix.common.data.locations.LocationsDao
import com.doberman.it.stuffix.common.data.travels.TravelsDao
import kotlinx.coroutines.flow.Flow

@Dao
abstract class ItemsDao {
    @Transaction
    @Query("SELECT * FROM ItemModelImpl WHERE archived = 0")
    abstract fun itemsAllWithCategoryAndLocationFlow(): Flow<List<ItemWithCategoryAndLocation>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun add(location: ItemModelImpl)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun add(locations: List<ItemModelImpl>)

    @Query("UPDATE ItemModelImpl SET archived = 1 WHERE id in (:itemIds)")
    abstract suspend fun archive(itemIds: Set<Long>)

    @Entity(
        foreignKeys = [ForeignKey(
            entity = LocationsDao.LocationModelImpl::class,
            parentColumns = ["id"],
            childColumns = ["locationId"],
            onDelete = ForeignKey.RESTRICT
        ), ForeignKey(
            entity = ItemCategoriesDao.ItemCategoriesModel::class,
            parentColumns = ["id"],
            childColumns = ["categoryId"],
            onDelete = ForeignKey.SET_NULL
        )],
        indices = [
            Index(value = ["categoryId"]),
            Index(value = ["locationId"])
        ]
    )
    class ItemModelImpl(
        @PrimaryKey(autoGenerate = true) override val id: Long,
        override val title: String,
        override val description: String?,
        override val categoryId: Long?,
        override val locationId: Long?,
        override val archived: Boolean = false,
        override val photo: String?
    ) : ItemModel {
        constructor(item: ItemModel) : this(
            item.id,
            item.title,
            item.description,
            item.categoryId,
            item.locationId,
            item.archived,
            item.photo
        )
    }

    class ItemWithCategoryAndLocation(
        @Embedded
        val itemModel: ItemModelImpl,
        @Relation(parentColumn = "categoryId", entityColumn = "id")
        val category: ItemCategoriesDao.ItemCategoriesModel,
        @Relation(parentColumn = "locationId", entityColumn = "id")
        val location: LocationsDao.LocationModelImpl
    )
}
