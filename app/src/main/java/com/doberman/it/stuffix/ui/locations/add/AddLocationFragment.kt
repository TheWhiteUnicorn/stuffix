/**
 * (c) 2020 Denis Erashov
 * This code is licensed under MIT license (see LICENSE.txt for details)
 */

package com.doberman.it.stuffix.ui.locations.add

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.observe
import androidx.navigation.fragment.findNavController
import com.doberman.it.stuffix.common.archutils.appViewModels
import com.doberman.it.stuffix.databinding.FragmentLocationAddBinding
import com.doberman.it.stuffix.ui.utils.popups.ExceptionHelper

/**
 * A fragment for add location screen
 *
 */
class AddLocationFragment : Fragment(),
    AddLocationViewModel.Navigation {
    private val viewModel: AddLocationViewModel by appViewModels {
        AddLocationViewModel(addLocation())
    }

    private lateinit var dataBinding: FragmentLocationAddBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dataBinding = FragmentLocationAddBinding.inflate(inflater, container, false)
        return dataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dataBinding.lifecycleOwner = viewLifecycleOwner
        dataBinding.viewModel = viewModel

        viewModel.navigation.observe(viewLifecycleOwner) { navFunction ->
            navFunction?.invoke(this@AddLocationFragment)
        }
        viewModel.state.observe(viewLifecycleOwner) { state ->
            when (state) {
                is AddLocationViewModel.State.Error -> ExceptionHelper.showSnack(view, state.throwable)
            }
        }
    }

    override fun navigateToLocationList() {
        val action =
            AddLocationFragmentDirections.actionAddLocationFragmentToNavigationLocations()
        findNavController().navigate(action)
    }
}
