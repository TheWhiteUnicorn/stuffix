package com.doberman.it.stuffix.ui.splash

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.doberman.it.stuffix.R
import com.doberman.it.stuffix.common.archutils.appViewModels
import com.doberman.it.stuffix.databinding.ActivitySplashBinding
import com.doberman.it.stuffix.ui.ARG_START_AUTHENTICATED
import com.doberman.it.stuffix.ui.MainActivity

class SplashActivity : AppCompatActivity() {
    private val viewModel: SplashViewModel by appViewModels {
        SplashViewModel(splash())
    }
    private lateinit var dataBinding: ActivitySplashBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        dataBinding = DataBindingUtil.setContentView(this, R.layout.activity_splash)

        viewModel.state.observe(this, Observer { state ->
            when (state) {
                is SplashViewModel.State.Error ->
                    AlertDialog.Builder(this)
                        .setTitle("Error")
                        .setMessage(state.throwable.message)
                        .setPositiveButton("OK") { _, _ ->
                            finishAndRemoveTask()
                        }
                        .create().show()
                is SplashViewModel.State.Success -> {
                    val intent = Intent(this, MainActivity::class.java)
                    intent.putExtra(ARG_START_AUTHENTICATED, state.isAuthenticated)
                    startActivity(intent)
                    finish()
                }
            }
        })
    }
}
