package com.doberman.it.stuffix.common.dagger

import com.doberman.it.stuffix.ui.MainActivityRepository
import com.doberman.it.stuffix.ui.MainActivityRepositoryImpl
import com.doberman.it.stuffix.ui.auth.signIn.SignInRepository
import com.doberman.it.stuffix.ui.auth.signIn.SignInRepositoryImpl
import com.doberman.it.stuffix.ui.auth.signUp.SignUpRepository
import com.doberman.it.stuffix.ui.auth.signUp.SignUpRepositoryImpl
import com.doberman.it.stuffix.ui.items.add.AddItemRepository
import com.doberman.it.stuffix.ui.items.add.AddItemRepositoryImpl
import com.doberman.it.stuffix.ui.items.list.ItemsListRepository
import com.doberman.it.stuffix.ui.items.list.ItemsListRepositoryImpl
import com.doberman.it.stuffix.ui.itemsSet.checklistDispatch.ItemsChecklistDispatchRepository
import com.doberman.it.stuffix.ui.itemsSet.checklistDispatch.ItemsChecklistDispatchRepositoryImpl
import com.doberman.it.stuffix.ui.itemsSet.make.ItemsSetRepository
import com.doberman.it.stuffix.ui.itemsSet.make.ItemsSetRepositoryImpl
import com.doberman.it.stuffix.ui.locations.add.AddLocationRepository
import com.doberman.it.stuffix.ui.locations.add.AddLocationRepositoryImpl
import com.doberman.it.stuffix.ui.locations.list.LocationsListRepository
import com.doberman.it.stuffix.ui.locations.list.LocationsListRepositoryImpl
import com.doberman.it.stuffix.ui.locations.view.ViewLocationRepository
import com.doberman.it.stuffix.ui.locations.view.ViewLocationRepositoryImpl
import com.doberman.it.stuffix.ui.splash.SplashRepository
import com.doberman.it.stuffix.ui.splash.SplashRepositoryImpl
import com.doberman.it.stuffix.ui.travels.add.AddTravelRepository
import com.doberman.it.stuffix.ui.travels.add.AddTravelRepositoryImpl
import com.doberman.it.stuffix.ui.travels.list.TravelsListRepository
import com.doberman.it.stuffix.ui.travels.list.TravelsListRepositoryImpl
import com.doberman.it.stuffix.ui.travels.view.ViewTravelRepository
import com.doberman.it.stuffix.ui.travels.view.ViewTravelRepositoryImpl
import dagger.Module
import dagger.Provides

@Module
class RepositoriesProvider {
    @Provides
    fun locationsListRepository(
        dao: DaoProvider
    ): LocationsListRepository =
        LocationsListRepositoryImpl(
            dao = dao.locationsDao()
        )

    @Provides
    fun addLocationRepository(
        dao: DaoProvider
    ): AddLocationRepository =
        AddLocationRepositoryImpl(
            dao = dao.locationsDao()
        )

    @Provides
    fun viewLocationRepository(
        dao: DaoProvider
    ): ViewLocationRepository =
        ViewLocationRepositoryImpl(
            dao = dao.locationsDao()
        )

    @Provides
    fun itemsListRepository(
        dao: DaoProvider
    ): ItemsListRepository =
        ItemsListRepositoryImpl(
            dao = dao.itemsDao()
        )

    @Provides
    fun addItemRepository(
        dao: DaoProvider
    ): AddItemRepository =
        AddItemRepositoryImpl(
            itemsDao = dao.itemsDao(),
            locationsDao = dao.locationsDao(),
            categoriesDao = dao.itemCategoriesDao()
        )

    @Provides
    fun travelsListRepository(
        dao: DaoProvider
    ): TravelsListRepository =
        TravelsListRepositoryImpl(
            dao = dao.travelsDao()
        )

    @Provides
    fun addTravelRepository(
        dao: DaoProvider
    ): AddTravelRepository =
        AddTravelRepositoryImpl(
            locationsDao = dao.locationsDao(),
            travelsDao = dao.travelsDao(),
            itemsSetDao = dao.itemsSet()
        )

    @Provides
    fun viewTravelRepository(
        dao: DaoProvider
    ): ViewTravelRepository =
        ViewTravelRepositoryImpl(
            travelsDao = dao.travelsDao(),
            itemsSetDao = dao.itemsSet(),
            transactionsDao = dao.itemTransactions()
        )

    @Provides
    fun splashRepository(
        dao: DaoProvider,
        sessionManager: SessionManager,
        gscProvider: GscProvider
    ): SplashRepository = SplashRepositoryImpl(
        dao = dao,
        sessionManager = sessionManager,
        gsc = gscProvider
    )

    @Provides
    fun itemsSetRepository(
        dao: DaoProvider
    ): ItemsSetRepository = ItemsSetRepositoryImpl(
        itemsDao = dao.itemsDao(),
        itemsSetDao = dao.itemsSet()
    )

    @Provides
    fun itemsChecklistDispatchRepository(
        dao: DaoProvider
    ): ItemsChecklistDispatchRepository = ItemsChecklistDispatchRepositoryImpl(
        travelsDao = dao.travelsDao(),
        itemsSetDao = dao.itemsSet(),
        transactionsDao = dao.itemTransactions()
    )

    @Provides
    fun signInRepository(
        retrofit: RetrofitProvider,
        sessionManager: SessionManager,
        gscProvider: GscProvider
    ): SignInRepository = SignInRepositoryImpl(
        retrofit = retrofit,
        sessionManager = sessionManager,
        gsc = gscProvider
    )

    @Provides
    fun signUpRepository(
        retrofit: RetrofitProvider,
        sessionManager: SessionManager
    ): SignUpRepository = SignUpRepositoryImpl(
        retrofit = retrofit,
        sessionManager = sessionManager
    )

    @Provides
    fun mainActivityRepository(
        sessionManager: SessionManager,
        gscProvider: GscProvider
    ): MainActivityRepository = MainActivityRepositoryImpl(
        sessionManager = sessionManager,
        gsc = gscProvider
    )
}