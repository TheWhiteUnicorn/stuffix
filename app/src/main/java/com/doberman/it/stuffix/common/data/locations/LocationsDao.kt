/**
 * (c) 2020 Denis Erashov
 * This code is licensed under MIT license (see LICENSE.txt for details)
 */

package com.doberman.it.stuffix.common.data.locations

import androidx.room.*
import kotlinx.coroutines.flow.Flow

@Dao
abstract class LocationsDao {
    /**
     * Get all non-archived locations
     *
     * @return list of plain location models wrapped in Flow
     */
    @Query("SELECT * FROM LocationModelImpl WHERE archived = 0")
    abstract fun locationsFlow(): Flow<List<LocationModelImpl>>

    /**
     * Get location by id
     *
     * @param id identifier of requested location
     * @return plain location model wrapped in Flow
     */
    @Query("SELECT * FROM LocationModelImpl WHERE id=:id")
    abstract fun locationFlow(id: Long): Flow<LocationModelImpl>

    /**
     * Add a new location to the DB
     *
     * @param location location to insert
     */
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    abstract suspend fun add(location: LocationModelImpl)

    /**
     * Add a list of new locations to the DB
     *
     * @param locations list of locations to insert
     */
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    abstract suspend fun add(locations: List<LocationModelImpl>)

    /**
     * Get a count of locations
     *
     * @return count of locations
     */
    @Query("SELECT COUNT(*) FROM LocationModelImpl")
    abstract suspend fun count(): Int

    /**
     * Mark a list of locations as archived
     *
     * @param locationIds ids of locations to archive
     */
    @Query("UPDATE LocationModelImpl SET archived = 1 WHERE id in (:locationIds)")
    abstract suspend fun archive(locationIds: Set<Long>)

    /**
     * Database model for locations
     *
     * @property id
     * @property title
     * @property description
     * @property address
     * @property archived true - if location is archived
     */
    @Entity
    class LocationModelImpl(
        @PrimaryKey(autoGenerate = true) override val id: Long = 0,
        override val title: String,
        override val description: String,
        override val address: String,
        override val archived: Boolean = false
    ) : LocationModel {
        constructor(location: LocationModel) : this(
            location.id,
            location.title,
            location.description,
            location.address,
            location.archived
        )
    }
}
