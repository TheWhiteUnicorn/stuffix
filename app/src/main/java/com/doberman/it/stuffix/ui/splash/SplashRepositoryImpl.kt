package com.doberman.it.stuffix.ui.splash

import com.doberman.it.stuffix.common.dagger.DaoProvider
import com.doberman.it.stuffix.common.dagger.GscProvider
import com.doberman.it.stuffix.common.dagger.SessionManager
import com.doberman.it.stuffix.common.data.itemCategories.ItemCategoriesDao
import com.doberman.it.stuffix.common.data.items.ItemsDao
import com.doberman.it.stuffix.common.data.locations.LocationsDao
import com.doberman.it.stuffix.common.data.travels.TravelModel
import com.doberman.it.stuffix.common.data.travels.TravelsDao
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import java.util.*

class SplashRepositoryImpl(
    dao: DaoProvider,
    private val sessionManager: SessionManager,
    private val gsc: GscProvider
) : SplashRepository {
    private val locations = dao.locationsDao()
    private val items = dao.itemsDao()
    private val travels = dao.travelsDao()
    private val itemCategories = dao.itemCategoriesDao()

    override suspend fun isEmpty(): Boolean = locations.count() == 0

    override suspend fun isAuthenticated(): Boolean {
        val account = gsc.actualizeSignedInAccount()
        val authenticatedWithGoogle = account != null
        val authenticatedWithEmailPassword = sessionManager.fetchAuthToken() != null

        return authenticatedWithEmailPassword || authenticatedWithGoogle
    }

    private fun constructDefaultLocations() = listOf(
        LocationsDao.LocationModelImpl(1, "Location 1", "Description description", "123"),
        LocationsDao.LocationModelImpl(2, "Location 2", "Description", "456")
    )

    private fun constructDefaultCategories() = listOf(
        ItemCategoriesDao.ItemCategoriesModel(1, "Tech"),
        ItemCategoriesDao.ItemCategoriesModel(2, "Hygiene")
    )

    private fun constructDefaultItems() = listOf(
        ItemsDao.ItemModelImpl(1, "Notebook", "description description", 1, 1, false, null),
        ItemsDao.ItemModelImpl(2, "Toothbrush", "description", 2, 1, false, null)
    )

    private fun constructDefaultTravel() = TravelsDao.TravelModelImpl(
        id = 1,
        title = "My awesome travel",
        plannedDispatchDateTime = GregorianCalendar(2020, 4, 28, 10, 0).time,
        plannedArriveDateTime = GregorianCalendar(2020, 4, 28, 10, 0).time,
        actualDispatchDateTime = GregorianCalendar(2020, 4, 28, 10, 0).time,
        actualArriveDateTime = GregorianCalendar(2020, 4, 28, 10, 0).time,
        startPointId = 1,
        destinationPointId = 2,
        note = "Note note note",
        stuffSetId = 1, // TODO: Constraint this
        status = TravelModel.TravelStatus.PLANNED
    )

    override suspend fun initCaches() {
        locations.add(constructDefaultLocations())
        itemCategories.add(constructDefaultCategories())
        items.add(constructDefaultItems())
        travels.add(constructDefaultTravel())
    }
}