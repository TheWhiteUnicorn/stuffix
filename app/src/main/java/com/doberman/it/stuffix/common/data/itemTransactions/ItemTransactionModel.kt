package com.doberman.it.stuffix.common.data.itemTransactions

interface ItemTransactionModel {
    val itemId: Long
    val travelId: Long
    val quantity: Int
    val isCompleted: Boolean
}