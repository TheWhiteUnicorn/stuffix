package com.doberman.it.stuffix.ui.itemsSet.make.tabContent

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.doberman.it.stuffix.common.archutils.appViewModels
import com.doberman.it.stuffix.databinding.FragmentItemsSetTabContentBinding
import com.doberman.it.stuffix.databinding.ItemSetItemForEditBinding
import com.doberman.it.stuffix.ui.itemsSet.make.ItemsSetViewModel
import com.doberman.it.stuffix.ui.utils.recycler.StaticRecyclerAdapter

enum class FilterOptions {
    ALL, TAKEN, LEFT
}

const val ARG_FILTER_OPTION = "filter option"

class MakeItemsSetTabContentFragment(
    private val parentViewModel: ItemsSetViewModel
) : Fragment() {
    private val viewModel: ItemsSetTabContentViewModel by appViewModels {
        ItemsSetTabContentViewModel(itemsSet(), parentViewModel, getFilterOptionArg() )
    }

    private lateinit var dataBinding: FragmentItemsSetTabContentBinding
    private lateinit var recyclerAdapter: StaticRecyclerAdapter<ItemSetItemForEditBinding>

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dataBinding = FragmentItemsSetTabContentBinding.inflate(inflater, container, false)
        return dataBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        dataBinding.lifecycleOwner = viewLifecycleOwner
        recyclerAdapter = StaticRecyclerAdapter(emptyList(), viewLifecycleOwner)
        dataBinding.itemsSetTabContentRecyclerView.adapter = recyclerAdapter

        viewModel.items.observe(viewLifecycleOwner, Observer {
            it?.let {recyclerAdapter.updateList(it.map(::SetItemSelectableCell))}
        })
    }

    private fun getFilterOptionArg(): FilterOptions {
        arguments?.takeIf {args ->
            args.containsKey(ARG_FILTER_OPTION).apply {
                return args.getSerializable(ARG_FILTER_OPTION) as FilterOptions?
                    ?: throw Exception("Unexpected filter option")
            }
        }
        throw Exception("Couldn't get filter argument")
    }
}