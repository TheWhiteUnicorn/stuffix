package com.doberman.it.stuffix.common.data.travels

import androidx.room.*
import com.doberman.it.stuffix.common.data.locations.LocationsDao
import kotlinx.coroutines.flow.Flow
import java.util.*


@Dao
abstract class TravelsDao {
    @Transaction
    @Query("SELECT * FROM TravelModelImpl")
    abstract fun allWithLocationsFlow(): Flow<List<TravelWithLocations>>

    @Transaction
    @Query("SELECT * FROM TravelModelImpl  WHERE id=:travelId")
    abstract fun getWithLocations(travelId: Long): Flow<TravelWithLocations>

    @Query("SELECT * FROM TravelModelImpl  WHERE id=:travelId")
    abstract fun getTravelCleanFlow(travelId: Long): Flow<TravelModelImpl>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    abstract suspend fun add(travel: TravelModelImpl)

    @TypeConverters(TravelStatusTypeConverter::class)
    @Query("UPDATE TravelModelImpl SET status = :status WHERE id = :id")
    abstract suspend fun updateStatus(id: Long, status: TravelModel.TravelStatus)

    @Entity(
        foreignKeys = [ForeignKey(
            entity = LocationsDao.LocationModelImpl::class,
            parentColumns = ["id"],
            childColumns = ["startPointId"],
            onDelete = ForeignKey.RESTRICT
        ), ForeignKey(
            entity = LocationsDao.LocationModelImpl::class,
            parentColumns = ["id"],
            childColumns = ["destinationPointId"],
            onDelete = ForeignKey.RESTRICT
        )],
        indices = [
            Index(value = ["startPointId"]),
            Index(value = ["destinationPointId"])
        ]
    )
    @TypeConverters(TravelStatusTypeConverter::class)
    class TravelModelImpl(
        @PrimaryKey(autoGenerate = true) override val id: Long,
        override val title: String,
        override val plannedDispatchDateTime: Date?,
        override val plannedArriveDateTime: Date?,
        override val actualDispatchDateTime: Date?,
        override val actualArriveDateTime: Date?,
        override val startPointId: Long,
        override val destinationPointId: Long,
        override val note: String?,
        override val stuffSetId: Long,
        override val status: TravelModel.TravelStatus
    ) : TravelModel {
        constructor(travel: TravelModel) : this(
            id = travel.id,
            title = travel.title,
            plannedDispatchDateTime = travel.plannedDispatchDateTime,
            plannedArriveDateTime = travel.plannedArriveDateTime,
            actualDispatchDateTime = travel.actualDispatchDateTime,
            actualArriveDateTime = travel.actualArriveDateTime,
            startPointId = travel.startPointId,
            destinationPointId = travel.destinationPointId,
            note = travel.note,
            stuffSetId = travel.stuffSetId,
            status = travel.status
        )
    }

    class TravelWithLocations(
        @Embedded
        val travelsModel: TravelModelImpl,
        @Relation(parentColumn = "startPointId", entityColumn = "id")
        val startPoint: LocationsDao.LocationModelImpl,
        @Relation(parentColumn = "destinationPointId", entityColumn = "id")
        val destinationPoint: LocationsDao.LocationModelImpl
    )

    class TravelStatusTypeConverter {
        @TypeConverter
        fun fromStringToTravelStatus(value: String): TravelModel.TravelStatus? =
            kotlin.runCatching { TravelModel.TravelStatus.valueOf(value) }.getOrNull()

        @TypeConverter
        fun travelStatusToString(status: TravelModel.TravelStatus): String = status.toString()
    }
}
