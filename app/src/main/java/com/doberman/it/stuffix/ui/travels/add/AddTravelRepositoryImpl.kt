package com.doberman.it.stuffix.ui.travels.add

import com.doberman.it.stuffix.common.data.itemsSet.ItemsSetDao
import com.doberman.it.stuffix.common.data.locations.LocationModel
import com.doberman.it.stuffix.common.data.locations.LocationsDao
import com.doberman.it.stuffix.common.data.travels.TravelModel
import com.doberman.it.stuffix.common.data.travels.TravelsDao
import kotlinx.coroutines.flow.Flow
import java.util.*

class AddTravelRepositoryImpl(
    private val locationsDao: LocationsDao,
    private val travelsDao: TravelsDao,
    private val itemsSetDao: ItemsSetDao
) : AddTravelRepository {
    override fun locationsFlow(): Flow<List<LocationModel>> = locationsDao.locationsFlow()

    override suspend fun addTravel(
        title: String,
        plannedDispatchDateTime: Date?,
        plannedArriveDateTime: Date?,
        startPointId: Long,
        destinationPointId: Long,
        note: String?
    ) {
        val itemsSetId = itemsSetDao.add(ItemsSetDao.ItemsSetModelImpl(id = 0))
        travelsDao.add(
            TravelsDao.TravelModelImpl(
                id = 0,
                title = title,
                plannedDispatchDateTime = plannedDispatchDateTime,
                plannedArriveDateTime = plannedArriveDateTime,
                startPointId = startPointId,
                destinationPointId = destinationPointId,
                note = note,
                status = TravelModel.TravelStatus.PLANNED,
                stuffSetId = itemsSetId,
                actualDispatchDateTime = null,
                actualArriveDateTime = null
            )
        )
    }
}