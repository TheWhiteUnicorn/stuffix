package com.doberman.it.stuffix.ui.items.list

class ItemSubViewModel(
    val model: ItemsListRepository.Item,
    val parent: ItemsListViewModel
) {
    fun onLongClick(): Boolean {
        if (parent._selectedElements != null)
            return false
        parent.itemLongChanged(model.header.id)
        return true
    }

    fun onSelectClick() = parent.onItemChanged(model.header.id)
}
