package com.doberman.it.stuffix.common.data.auth

interface UserProfile {
    val id: Long?
    val googleId: String?
    val email: String?
    val firstName: String?
    val lastName: String?
}