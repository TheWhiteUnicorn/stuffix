package com.doberman.it.stuffix.ui.auth.signIn

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.observe
import androidx.navigation.fragment.findNavController
import com.doberman.it.stuffix.RootNavigationGraphDirections
import com.doberman.it.stuffix.common.Constants
import com.doberman.it.stuffix.common.archutils.appViewModels
import com.doberman.it.stuffix.databinding.FragmentSignInBinding
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.common.SignInButton
import com.google.android.gms.tasks.Task


class SignInFragment : Fragment(), SignInViewModel.Navigation {
    private val viewModel: SignInViewModel by appViewModels {
        SignInViewModel(signIn())
    }
    private lateinit var dataBinding: FragmentSignInBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dataBinding = FragmentSignInBinding.inflate(inflater, container, false)
        return dataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        dataBinding.viewModel = viewModel
        dataBinding.lifecycleOwner = viewLifecycleOwner

        viewModel.navigation.observe(viewLifecycleOwner) { navFunction ->
            navFunction?.invoke(this@SignInFragment)
        }

        dataBinding.signInGoogleAuthButton.setSize(SignInButton.SIZE_WIDE)
        dataBinding.signInGoogleAuthButton.setOnClickListener {
            viewModel.onGoogleSignIn()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == Constants.ActivityRequestCodes.RC_GOOGLE_SIGN_IN) {

            val task: Task<GoogleSignInAccount> = GoogleSignIn.getSignedInAccountFromIntent(data)
            viewModel.handleSignInResult(task)
        }
    }

    override fun startGoogleSignIn(intent: Intent) {
        startActivityForResult(intent, Constants.ActivityRequestCodes.RC_GOOGLE_SIGN_IN)
    }

    override fun navigateToSignIn() {
        val action =
            SignInFragmentDirections.actionNavigationFragmentSignInToNavigationFragmentSignUp()
        findNavController().navigate(action)
    }

    override fun navigateToHomeScreen() {
        val action = RootNavigationGraphDirections.actionGlobalNavigationFragmentHomeScreen()
        findNavController().navigate(action)
    }
}
