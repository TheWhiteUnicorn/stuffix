package com.doberman.it.stuffix.ui.travels.add

import com.doberman.it.stuffix.common.data.locations.LocationModel
import com.doberman.it.stuffix.common.data.travels.TravelModel
import kotlinx.coroutines.flow.Flow
import java.util.*

interface AddTravelRepository {
    fun locationsFlow(): Flow<List<LocationModel>>
    suspend fun addTravel(
        title: String,
        plannedDispatchDateTime: Date?,
        plannedArriveDateTime: Date?,
        startPointId: Long,
        destinationPointId: Long,
        note: String?
    )
}