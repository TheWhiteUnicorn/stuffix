package com.doberman.it.stuffix.ui

import androidx.lifecycle.ViewModel

class MainActivityViewModel(
    val repository: MainActivityRepository
) : ViewModel() {
    val userProfile = repository.getUserProfile()
    fun suspendAuthenticatedSession() = repository.suspendAuthenticatedSession()
    fun actualizeSignedInAccount() = repository.actualizeSignedInAccount()
}