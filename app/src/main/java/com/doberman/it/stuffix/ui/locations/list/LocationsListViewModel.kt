/**
 * (c) 2020 Denis Erashov
 * This code is licensed under MIT license (see LICENSE.txt for details)
 */

package com.doberman.it.stuffix.ui.locations.list

import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.doberman.it.stuffix.common.archutils.LiveProperty
import com.doberman.it.stuffix.common.archutils.SingleHandledEvent
import com.doberman.it.stuffix.ui.utils.selectableList.SelectionActionModeCallback
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch

/**
 * A view model for locations list screen
 *
 * @property locationsListRepository
 */
class LocationsListViewModel(
    private val locationsListRepository: LocationsListRepository
) : ViewModel(), SelectionActionModeCallback.Actions {
    val navigation = SingleHandledEvent<(Navigation) -> Unit>()

    val locations = locationsListRepository
        .locationsFlow()
        .map { list -> list.map { LocationSubViewModel(it, this) } }
        .asLiveData(viewModelScope.coroutineContext)

    // A set of ids of selected locations
    val selectedElements = LiveProperty<Set<Long>?>(null)
    internal var _selectedElements: Set<Long>? by selectedElements

    /**
     * Actions that should be performed when submit floating action button is pressed
     *
     */
    fun onFabPress() {
        navigation.value = { it.navigateToAddLocation() }
    }

    /**
     * Actions that should be performed when user long presses on one of items
     *
     * @param modelId id of location that's being displayed in pressed cell
     */
    fun itemLongChanged(modelId: Long) {
        _selectedElements = (_selectedElements ?: emptySet()) + modelId
    }

    /**
     * Actions that should be performed when user presses on one of items
     *
     * @param dataId id of location that's being displayed in pressed cell
     */
    fun onItemPressed(dataId: Long) {
        if (_selectedElements == null) {
            navigation.value = { it.navigateToViewLocation(dataId)}
        } else {
            val selectedElements = _selectedElements ?: return
            _selectedElements = if (dataId in selectedElements)
                selectedElements - dataId
            else
                selectedElements + dataId
        }
    }

    override fun cancelSelection() {
        _selectedElements = null
    }

    override fun archiveSelected() {
        viewModelScope.launch {
            locationsListRepository.archiveLocations(_selectedElements ?: return@launch)
            _selectedElements = null
        }
    }

    /**
     * Navigation actions, controlled by this view model
     *
     */
    interface Navigation {
        fun navigateToAddLocation()
        fun navigateToViewLocation(locationId: Long)
    }
}