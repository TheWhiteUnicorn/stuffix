package com.doberman.it.stuffix.common.data.itemsSet

interface ItemInItemsSetModel {
    val itemId: Long
    val itemsSetId: Long
    val quantity: Int
}