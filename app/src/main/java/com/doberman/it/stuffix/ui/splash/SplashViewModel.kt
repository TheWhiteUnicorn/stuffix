package com.doberman.it.stuffix.ui.splash

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.doberman.it.stuffix.common.archutils.LiveProperty
import kotlinx.coroutines.async
import kotlinx.coroutines.launch

class SplashViewModel(
    private val repository: SplashRepository
) : ViewModel() {
    val state = LiveProperty<State>(State.Loading)
    private var _state: State by state

    init {
        viewModelScope.launch {
            _state = try {
                val initCachesJob = async { initCaches() }
                val checkAuthJob = async { checkAuth() }

                initCachesJob.await()
                val initAuthResult = checkAuthJob.await()
                State.Success(initAuthResult)
            } catch (e: Exception) {
                State.Error(e)
            }
        }
    }

    private suspend fun initCaches() {
        if (repository.isEmpty())
            repository.initCaches()
    }


    private suspend fun checkAuth() = repository.isAuthenticated()


    sealed class State {
        object Loading : State()
        class Error(val throwable: Throwable) : State()
        class Success(val isAuthenticated: Boolean) : State()
    }
}