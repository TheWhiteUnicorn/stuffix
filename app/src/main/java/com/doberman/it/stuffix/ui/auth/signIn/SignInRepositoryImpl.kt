package com.doberman.it.stuffix.ui.auth.signIn

import com.doberman.it.stuffix.common.dagger.GscProvider
import com.doberman.it.stuffix.common.dagger.RetrofitProvider
import com.doberman.it.stuffix.common.dagger.SessionManager
import com.doberman.it.stuffix.common.data.auth.AuthApiService
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class SignInRepositoryImpl(
    private val retrofit: RetrofitProvider,
    private val sessionManager: SessionManager,
    private val gsc: GscProvider
) : SignInRepository {
    override suspend fun login(email: String, password: String) {
        val response = retrofit.auth().basicLogin(AuthApiService.LoginRequest(email, password))

        val loginResponse = response.body()
        if (response.code() == 200) {
            loginResponse?.let {
                if (loginResponse.data?.token != null) {
                    sessionManager.saveAuthToken(loginResponse.data.token)
                    sessionManager.setUserProfile(
                        SessionManager.UserProfileImpl(
                            id = loginResponse.data.id,
                            email = loginResponse.data.email,
                            firstName = loginResponse.data.firstName,
                            lastName = loginResponse.data.lastName
                        )
                    )
                    return
                } else {
                    throw Exception(loginResponse.error)
                }
            } ?: throw Exception("Networking error")
        } else {
            val type = object : TypeToken<AuthApiService.LoginResponse>() {}.type
            val errorResponse: AuthApiService.LoginResponse? =
                Gson().fromJson(response.errorBody()!!.charStream(), type)
            throw Exception(errorResponse?.error)
        }
    }

    override fun getGsc(): GoogleSignInClient? = gsc.googleSignInClient

    override fun actualizeGsa() {
        gsc.actualizeSignedInAccount()
    }
}