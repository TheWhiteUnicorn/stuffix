/**
 * (c) 2020 Denis Erashov
 * This code is licensed under MIT license (see LICENSE.txt for details)
 */

package com.doberman.it.stuffix.ui.utils.recycler

import androidx.annotation.LayoutRes
import androidx.databinding.ViewDataBinding


/**
 * An interface of a container that stores DataBinding of cells for Static Recycler
 *
 * @param T type of generated layout databinding class
 */
interface IDataBindingView<out T : ViewDataBinding> {
    @get:LayoutRes
    val layoutID: Int

    /**
     * Operations that should be done with binding instance when cell is bound
     *
     * @param binding cell binding instance
     */
    fun bindTo(binding: @UnsafeVariance T)

    /**
     * Operations that should be done with binding instance when cell is unbound
     *
     * @param binding cell binding instance
     */
    fun unbindFrom(binding: @UnsafeVariance T)
}
