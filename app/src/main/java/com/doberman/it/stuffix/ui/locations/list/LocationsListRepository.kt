package com.doberman.it.stuffix.ui.locations.list

import com.doberman.it.stuffix.common.data.locations.LocationModel
import kotlinx.coroutines.flow.Flow

interface LocationsListRepository {
    fun locationsFlow():Flow<List<LocationModel>>

    suspend fun archiveLocations(locationIds: Set<Long>)
}