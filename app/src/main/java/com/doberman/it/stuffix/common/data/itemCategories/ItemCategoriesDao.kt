package com.doberman.it.stuffix.common.data.itemCategories

import androidx.room.*
import kotlinx.coroutines.flow.Flow

@Dao
abstract class ItemCategoriesDao {
    @Query("SELECT * FROM ItemCategoriesModel")
    abstract fun categoriesFlow(): Flow<List<ItemCategoriesModel>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun add(itemCategory: ItemCategoriesModel)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun add(itemCategory: List<ItemCategoriesModel>)

    @Entity
    class ItemCategoriesModel(
        @PrimaryKey override val id: Long,
        override val title: String
    ) : ItemCategory
}
