package com.doberman.it.stuffix.ui.auth.signUp

import com.doberman.it.stuffix.common.dagger.RetrofitProvider
import com.doberman.it.stuffix.common.dagger.SessionManager
import com.doberman.it.stuffix.common.data.auth.AuthApiService
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class SignUpRepositoryImpl(
    private val retrofit: RetrofitProvider,
    private val sessionManager: SessionManager
) : SignUpRepository {
    override suspend fun register(
        email: String,
        password: String,
        givenName: String,
        familyName: String
    ) {
        val response = retrofit.auth().register(AuthApiService.RegistrationRequest(email, password, givenName, familyName))

        val registerResponse = response.body()
        if (response.code() == 201) {
            registerResponse?.let {
                if (registerResponse.data?.token != null) {
                    sessionManager.saveAuthToken(registerResponse.data.token)
                    sessionManager.setUserProfile(
                        SessionManager.UserProfileImpl(
                            id = registerResponse.data.id,
                            email = registerResponse.data.email,
                            firstName = registerResponse.data.firstName,
                            lastName = registerResponse.data.lastName
                        )
                    )
                    return
                } else {
                    throw Exception(registerResponse.error)
                }
            } ?: throw Exception("Networking error")
        } else {
            val type = object : TypeToken<AuthApiService.LoginResponse>() {}.type
            val errorResponse: AuthApiService.LoginResponse? =
                Gson().fromJson(response.errorBody()!!.charStream(), type)
            throw Exception(errorResponse?.error)
        }
    }
}