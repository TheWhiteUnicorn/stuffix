package com.doberman.it.stuffix.ui.itemsSet.make

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.observe
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.viewpager2.widget.ViewPager2
import com.doberman.it.stuffix.R
import com.doberman.it.stuffix.common.archutils.appViewModels
import com.doberman.it.stuffix.databinding.FragmentItemsSetBinding
import com.google.android.material.tabs.TabLayoutMediator

class ItemsSetFragment : Fragment(), ItemsSetViewModel.Navigation {
    private val args: ItemsSetFragmentArgs by navArgs()
    val viewModel: ItemsSetViewModel by appViewModels {
        ItemsSetViewModel(itemsSet(), args.itemsSetId)
    }

    private lateinit var dataBinding: FragmentItemsSetBinding
    private lateinit var stateAdapter: ItemsSetStateAdapter
    private lateinit var viewPager: ViewPager2

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dataBinding = FragmentItemsSetBinding.inflate(inflater, container, false)
        return dataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dataBinding.viewModel = viewModel
        stateAdapter = ItemsSetStateAdapter(this)
        viewPager = dataBinding.itemsSetPager
        val tabLayout = dataBinding.itemsSetTabLayout
        viewPager.adapter = stateAdapter
        TabLayoutMediator(tabLayout, viewPager) { tab, position ->
            tab.text = when(position){
                0 -> getString(R.string.tab_text_all)
                1 -> getString(R.string.tab_text_taken)
                2 -> getString(R.string.tab_text_left)
                else -> throw Exception("Unhandled position")
            }
        }.attach()

        viewModel.navigation.observe(viewLifecycleOwner) { it?.invoke(this) }
    }

    override fun navigateToViewTravel() {
        findNavController().navigateUp()
    }
}