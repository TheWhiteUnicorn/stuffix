package com.doberman.it.stuffix.ui.itemsSet.checklistDispatch

import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.doberman.it.stuffix.common.archutils.LiveProperty
import com.doberman.it.stuffix.common.archutils.SingleHandledEvent
import com.doberman.it.stuffix.ui.itemsSet.make.ItemsSetViewModel
import com.doberman.it.stuffix.ui.travels.view.ViewTravelViewModel
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch

class ItemsCheckLIstDispatchViewModel(
    private val repository: ItemsChecklistDispatchRepository,
    private val travelId: Long
): ViewModel() {
    val navigation = SingleHandledEvent<(Navigation) -> Unit>()

    val itemsToTake = repository
        .getSetItemsListFlow(travelId)
        .map { list -> list.map { ItemSubViewModel(it, this) } }
        .asLiveData()

    val selectedElements = LiveProperty<Set<SelectedItemData>>(emptySet())
    internal var _selectedElements: Set<SelectedItemData> by selectedElements

    fun onItemChanged(dataId: Long) {
        val maybeExistingElement = _selectedElements.firstOrNull { it.id == dataId }
        _selectedElements = if (maybeExistingElement != null)
            _selectedElements - maybeExistingElement
        else
            _selectedElements + SelectedItemData(dataId, 1)
    }

    fun onSubmitPress() = viewModelScope.launch {
        repository.createItemsTransactions(_selectedElements, travelId)
        repository.setTravelStatusActive(travelId)
        navigation.value = {it.navigateToViewTravel()}
    }

    interface Navigation {
        fun navigateToViewTravel()
    }

    class SelectedItemData(
        val id: Long,
        val quantity: Int
    )
}