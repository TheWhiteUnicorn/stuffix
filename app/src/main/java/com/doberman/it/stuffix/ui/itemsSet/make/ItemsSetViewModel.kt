package com.doberman.it.stuffix.ui.itemsSet.make

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.doberman.it.stuffix.common.archutils.LiveProperty
import com.doberman.it.stuffix.common.archutils.SingleHandledEvent
import kotlinx.coroutines.launch

class ItemsSetViewModel(
    private val repository: ItemsSetRepository,
    private val itemsSetId: Long
) : ViewModel() {
    val navigation = SingleHandledEvent<(Navigation) -> Unit>()

    val selectedElements = LiveProperty<Set<SelectedItemData>>(emptySet())
    internal var _selectedElements: Set<SelectedItemData> by selectedElements

    fun onItemPressed(dataId: Long) {
        val maybeExistingElement = _selectedElements.firstOrNull { it.id == dataId }
        _selectedElements = if (maybeExistingElement != null)
            _selectedElements - maybeExistingElement
        else
            _selectedElements + SelectedItemData(dataId, 1)
    }

    fun onSubmitPressed() = viewModelScope.launch {
        repository.updateItemsSet(_selectedElements, itemsSetId)
        navigation.value = { it.navigateToViewTravel() }
    }

    interface Navigation {
        fun navigateToViewTravel()
    }

    class SelectedItemData(
        val id: Long,
        val quantity: Int
    )
}