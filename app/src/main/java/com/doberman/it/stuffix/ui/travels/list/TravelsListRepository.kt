package com.doberman.it.stuffix.ui.travels.list

import com.doberman.it.stuffix.common.data.locations.LocationModel
import com.doberman.it.stuffix.common.data.travels.TravelsDao
import kotlinx.coroutines.flow.Flow

interface TravelsListRepository {
    fun getTravelsList(): Flow<List<Travel>>

    class Travel(
        val header: TravelsDao.TravelModelImpl,
        val startPoint: LocationModel,
        val destinationPoint: LocationModel
    )
}
