package com.doberman.it.stuffix.ui.itemsSet.checklistDispatch

import com.doberman.it.stuffix.common.data.itemTransactions.ItemTransactionsDao
import com.doberman.it.stuffix.common.data.itemsSet.ItemsSetDao
import com.doberman.it.stuffix.common.data.travels.TravelModel
import com.doberman.it.stuffix.common.data.travels.TravelsDao
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow

class ItemsChecklistDispatchRepositoryImpl(
    private val travelsDao: TravelsDao,
    private val itemsSetDao: ItemsSetDao,
    private val transactionsDao: ItemTransactionsDao
) : ItemsChecklistDispatchRepository {

    override fun getSetItemsListFlow(travelId: Long): Flow<List<ItemsChecklistDispatchRepository.SetItem>> {
        return flow {
            travelsDao.getTravelCleanFlow(travelId).collect { travel ->
                itemsSetDao.getSetWithItems(travel.stuffSetId).collect { itemsSet ->
                    emit(itemsSet.itemsInSet.map { setItemWithItem ->
                        ItemsChecklistDispatchRepository.SetItem(
                            header = setItemWithItem.itemWithCategoryAndLocation.itemModel,
                            category = setItemWithItem.itemWithCategoryAndLocation.category,
                            quantity = setItemWithItem.setItem.quantity
                        )
                    })
                }
            }
        }
    }

    override suspend fun createItemsTransactions(
        takenItems: Set<ItemsCheckLIstDispatchViewModel.SelectedItemData>,
        travelId: Long
    ) {
        val transactions = takenItems.mapTo(HashSet()) { takenItem ->
            ItemTransactionsDao.ItemTransactionModelImpl(
                itemId = takenItem.id,
                travelId = travelId,
                quantity = takenItem.quantity,
                isCompleted = false
            )
        }
        transactionsDao.addTransactions(transactions)
    }

    override suspend fun setTravelStatusActive(travelId: Long) {
        travelsDao.updateStatus(travelId, TravelModel.TravelStatus.ACTIVE)
    }
}