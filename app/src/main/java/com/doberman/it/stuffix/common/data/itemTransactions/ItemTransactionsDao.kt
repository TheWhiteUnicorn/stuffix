package com.doberman.it.stuffix.common.data.itemTransactions

import androidx.room.*

@Dao
abstract class ItemTransactionsDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun addTransactions(transactions: Set<ItemTransactionModelImpl>)

    @Query("UPDATE ItemTransactionModelImpl SET isCompleted = 1 WHERE travelId = :travelId")
    abstract suspend fun finishTransactions(travelId: Long)


    @Entity(primaryKeys = ["itemId", "travelId"])
    class ItemTransactionModelImpl(
        override val itemId: Long,
        override val travelId: Long,
        override val quantity: Int,
        override val isCompleted: Boolean
    ) : ItemTransactionModel
}