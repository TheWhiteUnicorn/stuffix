package com.doberman.it.stuffix.common.data.travels

import java.util.*

interface TravelModel {
    val id: Long
    val title: String
    val plannedDispatchDateTime: Date?
    val plannedArriveDateTime: Date?
    val actualDispatchDateTime: Date?
    val actualArriveDateTime: Date?
    val startPointId: Long
    val destinationPointId: Long
    val note: String?
    val stuffSetId: Long
    val status: TravelStatus

    enum class TravelStatus {
        PLANNED,
        ACTIVE,
        FINISHED,
        CANCELED,
    }
}
