/**
 * (c) 2020 Denis Erashov
 * This code is licensed under MIT license (see LICENSE.txt for details)
 */

package com.doberman.it.stuffix.ui.locations.add

import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.doberman.it.stuffix.common.archutils.LiveProperty
import com.doberman.it.stuffix.common.data.locations.LocationsDao
import com.doberman.it.stuffix.common.archutils.SingleHandledEvent
import kotlinx.coroutines.launch

/**
 * A view model for location adding screen
 *
 * @property addLocationRepository
 */
class AddLocationViewModel(
    private val addLocationRepository: AddLocationRepository
) : ViewModel() {
    // Navigation events exposed to and processed by corresponding fragment
    val navigation = SingleHandledEvent<(Navigation) -> Unit>()

    // Live updated current state of screen
    val state = LiveProperty<State>(State.Normal)
    private var _state: State by state

    // Location title
    val title = MutableLiveData<String>()
    // Location address
    val address = MutableLiveData<String>()
    // Location description
    val description = MutableLiveData<String>()

    // true - if form is valid and it's possible to submit the data
    val isValid = MediatorLiveData<Boolean>().apply {
        fun validate() {
            this.value = title.value?.isNotEmpty() == true
                    && address.value?.isNotEmpty() == true
                    && description.value?.isNotEmpty() == true
        }

        this.addSource(title) { validate() }
        this.addSource(address) { validate() }
        this.addSource(description) { validate() }
    }

    /**
     * A callback that UI calls when user confirms new location creation
     *
     */
    fun onAddLocation() = viewModelScope.launch {
        _state = State.Adding
        try {
            addLocationRepository.addLocation(
                LocationsDao.LocationModelImpl(
                    id = 0,
                    title = title.value?.takeIf { it.isNotEmpty() }
                        ?: throw Exception("Missing requited title value"),
                    description = description.value?.takeIf { it.isNotEmpty() }
                        ?: throw Exception("Missing requited description value"),
                    address = address.value?.takeIf { it.isNotEmpty() }
                        ?: throw Exception("Missing requited address value")
                )
            )
        } catch (t: Throwable) {
            _state = State.Error(t)
            return@launch
        }
        _state = State.Normal
        navigation.value = { it.navigateToLocationList() }
    }

    /**
     * Navigation actions, controlled by this view model
     *
     */
    interface Navigation {
        fun navigateToLocationList()
    }

    /**
     * Possible screen states:
     * Normal - able to fill form and make submission
     * Adding - submission is in progress, UI blocked - the application creates a new location now
     * Error - some kind of exception appeared while submitting - user should see error message and fix it to submit again
     */
    sealed class State {
        object Normal : State()
        object Adding : State()
        class Error(val throwable: Throwable) : State()
    }
}