package com.doberman.it.stuffix.ui.travels.view.takenItemsSection

import com.doberman.it.stuffix.R
import com.doberman.it.stuffix.databinding.ItemSetItemForDisplayBinding
import com.doberman.it.stuffix.ui.utils.recycler.IDataBindingView

class SetItemCell(
    private val viewModel: SetItemSubViewModel
) : IDataBindingView<ItemSetItemForDisplayBinding> {
    override val layoutID: Int = R.layout.item_set_item_for_display

    override fun bindTo(binding: ItemSetItemForDisplayBinding) {
        binding.viewModel = viewModel
    }

    override fun unbindFrom(binding: ItemSetItemForDisplayBinding) = Unit


}