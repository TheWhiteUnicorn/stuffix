package com.doberman.it.stuffix.ui.travels.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.observe
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.doberman.it.stuffix.common.archutils.appViewModels
import com.doberman.it.stuffix.databinding.FragmentTravelViewBinding
import com.doberman.it.stuffix.databinding.ItemSetItemForDisplayBinding
import com.doberman.it.stuffix.ui.travels.view.takenItemsSection.SetItemCell
import com.doberman.it.stuffix.ui.utils.recycler.StaticRecyclerAdapter

class ViewTravelFragment : Fragment(), ViewTravelViewModel.Navigation {
    private val args: ViewTravelFragmentArgs by navArgs()
    private val viewModel: ViewTravelViewModel by appViewModels {
        ViewTravelViewModel(viewTravel(), args.travelId)
    }

    private lateinit var dataBinding: FragmentTravelViewBinding
    private lateinit var recyclerAdapter: StaticRecyclerAdapter<ItemSetItemForDisplayBinding>

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dataBinding = FragmentTravelViewBinding.inflate(inflater, container, false)
        return dataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dataBinding.lifecycleOwner = viewLifecycleOwner
        dataBinding.viewModel = viewModel

        recyclerAdapter = StaticRecyclerAdapter(emptyList(), viewLifecycleOwner)
        dataBinding.travelViewStuffRecycler.adapter = recyclerAdapter

        viewModel.takenItems.observe(viewLifecycleOwner, Observer {
            it?.let { recyclerAdapter.updateList(it.map(::SetItemCell)) }
        })

        viewModel.navigation.observe(viewLifecycleOwner) { it?.invoke(this) }
    }

    override fun navigateToItemsSet(itemsSetId: Long) {
        val action = ViewTravelFragmentDirections.actionViewTravelToItemsSet(itemsSetId)
        findNavController().navigate(action)
    }

    override fun navigateToDispatchChecklist(travelId: Long) {
        val action = ViewTravelFragmentDirections.actionViewTravelToItemsChecklistDispatch(travelId)
        findNavController().navigate(action)
    }
}