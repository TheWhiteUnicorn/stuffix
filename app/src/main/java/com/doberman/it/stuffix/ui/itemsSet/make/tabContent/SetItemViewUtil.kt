package com.doberman.it.stuffix.ui.itemsSet.make.tabContent

import com.doberman.it.stuffix.ui.itemsSet.make.ItemsSetViewModel

object SetItemViewUtil {
    @JvmStatic
    fun checked(modelId: Long, selectedIds: Set<ItemsSetViewModel.SelectedItemData>?): Boolean =
        selectedIds?.any { it.id == modelId } == true
}