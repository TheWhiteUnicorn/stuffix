package com.doberman.it.stuffix.ui.items.list

import com.doberman.it.stuffix.common.data.items.ItemsDao
import com.doberman.it.stuffix.common.data.items.ItemModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class ItemsListRepositoryImpl(
    private val dao: ItemsDao
) : ItemsListRepository {
    override fun getItemsFlow(): Flow<List<ItemsListRepository.Item>> =
        dao.itemsAllWithCategoryAndLocationFlow()
            .map { list ->
                list.map {
                    ItemsListRepository.Item(
                        it.itemModel,
                        it.location,
                        it.category
                    )
                }
            }

    override suspend fun archiveItems(itemsIds: Set<Long>) = dao.archive(itemsIds)
}