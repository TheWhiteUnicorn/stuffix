package com.doberman.it.stuffix.ui.itemsSet.checklistDispatch

import com.doberman.it.stuffix.R
import com.doberman.it.stuffix.databinding.ItemSetItemForChecklistBinding
import com.doberman.it.stuffix.ui.items.list.ItemSubViewModel
import com.doberman.it.stuffix.ui.utils.recycler.IDataBindingView

class ItemSelectableCell(
    private val viewModel: com.doberman.it.stuffix.ui.itemsSet.checklistDispatch.ItemSubViewModel
) : IDataBindingView<ItemSetItemForChecklistBinding> {
    override val layoutID: Int = R.layout.item_set_item_for_checklist

    override fun bindTo(binding: ItemSetItemForChecklistBinding) {
        binding.viewModel = viewModel
    }

    override fun unbindFrom(binding: ItemSetItemForChecklistBinding) = Unit
}