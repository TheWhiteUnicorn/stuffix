package com.doberman.it.stuffix.ui.auth.signIn

import com.google.android.gms.auth.api.signin.GoogleSignInClient

interface SignInRepository {
    suspend fun login(email: String, password: String)
    fun getGsc(): GoogleSignInClient?
    fun actualizeGsa()
}