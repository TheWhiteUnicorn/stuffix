package com.doberman.it.stuffix.ui.auth.signUp

import android.content.Context
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.doberman.it.stuffix.R
import com.doberman.it.stuffix.common.archutils.LiveProperty
import com.doberman.it.stuffix.common.archutils.SingleHandledEvent
import com.doberman.it.stuffix.ui.auth.signIn.SignInViewModel
import com.doberman.it.stuffix.ui.utils.Validation
import kotlinx.coroutines.launch


class SignUpViewModel(
    private val repository: SignUpRepository
) : ViewModel() {
    val navigation = SingleHandledEvent<(Navigation) -> Unit>()

    val state = LiveProperty<State>(State.Normal)
    private var _state: State by state

    val email = MutableLiveData<String>()
    val password = MutableLiveData<String>()
    val password2 = MutableLiveData<String>()
    val givenName = MutableLiveData<String>()
    val familyName = MutableLiveData<String>()

    val isValid = MediatorLiveData<Boolean>().apply {
        fun validate() {
            this.value = _state != State.Processing

                    && Validation.isEmailValid(email.value)
                    && password.value == password2.value
                    && Validation.isPasswordValid(password.value)
                    && givenName.value?.isNotEmpty() == true
                    && familyName.value?.isNotEmpty() == true
        }

        this.addSource(state) { validate() }
        this.addSource(email) { validate() }
        this.addSource(password) { validate() }
        this.addSource(password2) { validate() }
        this.addSource(givenName) { validate() }
        this.addSource(familyName) { validate() }
    }

    fun onSignUpSubmit() = viewModelScope.launch {
        _state = State.Processing
        try {
            repository.register(
                email = email.value ?: throw MissingFieldValueException(
                    MissingFieldValueException.FieldType.Email
                ),
                password = password.value ?: throw MissingFieldValueException(
                    MissingFieldValueException.FieldType.Password
                ),
                givenName = givenName.value ?: throw MissingFieldValueException(
                    MissingFieldValueException.FieldType.GivenName
                ),
                familyName = familyName.value ?: throw MissingFieldValueException(
                    MissingFieldValueException.FieldType.FamilyName
                )
            )
        } catch (t: Throwable) {
            _state = State.Error(t)
            return@launch
        }
        _state = State.Normal
        navigation.value = { it.navigateToHomeScreen() }
    }

    fun handleNavigateToSignIn() {
        navigation.value = { it.navigateToSignIn() }
    }

    interface Navigation {
        fun navigateToSignIn()
        fun navigateToHomeScreen()
    }

    sealed class State {
        object Normal : State()
        object Processing : State()
        class Error(val throwable: Throwable) : State()
    }

    class MissingFieldValueException(val field: FieldType) : Throwable() {
        sealed class FieldType {
            object Email : FieldType()
            object Password : FieldType()
            object GivenName : FieldType()
            object FamilyName : FieldType()
        }
    }
}
