package com.doberman.it.stuffix.ui.travels.view

import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.doberman.it.stuffix.common.archutils.SingleHandledEvent
import com.doberman.it.stuffix.common.data.travels.TravelModel
import com.doberman.it.stuffix.common.data.travels.TravelsDao
import com.doberman.it.stuffix.ui.travels.list.TravelSubViewModel
import com.doberman.it.stuffix.ui.travels.view.takenItemsSection.SetItemSubViewModel
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import java.lang.Exception

class ViewTravelViewModel(
    val viewTravelRepository: ViewTravelRepository,
    travelId: Long
) : ViewModel() {
    val navigation = SingleHandledEvent<(Navigation) -> Unit>()

    val travel = viewTravelRepository.getTravelData(travelId).asLiveData()

    val takenItems = viewTravelRepository
        .getTakenItemsData(travelId)
        .map { list -> list.map { SetItemSubViewModel(it, this) } }
        .asLiveData()

    fun onEditItemsSet() {
        travel.value?.let { value ->
            navigation.value = { it.navigateToItemsSet(value.header.stuffSetId) }
        }
    }

    fun onFabPress() {
        travel.value?.let { travel ->
            when (travel.header.status) {
                TravelModel.TravelStatus.PLANNED -> navigation.value =
                    { it.navigateToDispatchChecklist(travel.header.id) }
                TravelModel.TravelStatus.ACTIVE -> viewModelScope.launch {
                    viewTravelRepository.markTransactionsFinished(
                        travel.header.id
                    )
                    viewTravelRepository.setTravelStatusFinished(travel.header.id)
                }
                else -> throw Exception("Unhandled travel status")
            }
        }
    }

    interface Navigation {
        fun navigateToItemsSet(itemsSetId: Long)
        fun navigateToDispatchChecklist(travelId: Long)
    }
}