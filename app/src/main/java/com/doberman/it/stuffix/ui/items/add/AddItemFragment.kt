package com.doberman.it.stuffix.ui.items.add

import android.app.Activity.RESULT_OK
import android.content.Intent
import android.database.Cursor
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.fragment.app.Fragment
import androidx.lifecycle.observe
import androidx.navigation.fragment.findNavController
import com.doberman.it.stuffix.common.Constants
import com.doberman.it.stuffix.common.archutils.appViewModels
import com.doberman.it.stuffix.common.data.itemCategories.ItemCategory
import com.doberman.it.stuffix.common.data.locations.LocationModel
import com.doberman.it.stuffix.databinding.FragmentTravelItemAddBinding
import com.doberman.it.stuffix.ui.utils.checkStorageRwPermissions


class AddItemFragment : Fragment(),
    AddItemViewModel.Navigation {
    private val viewModel: AddItemViewModel by appViewModels {
        AddItemViewModel(addItem())
    }

    private lateinit var dataBinding: FragmentTravelItemAddBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dataBinding = FragmentTravelItemAddBinding.inflate(inflater, container, false)
        return dataBinding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        dataBinding.lifecycleOwner = viewLifecycleOwner
        dataBinding.viewModel = viewModel

        dataBinding.locationSpinner.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) = Unit

                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) = viewModel.onSelectLocation(position)
            }

        dataBinding.categorySpinner.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) = Unit

                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) = viewModel.onSelectCategory(position)
            }

        viewModel.locations.observe(viewLifecycleOwner) {
            displayLocations(it)
        }
        viewModel.categories.observe(viewLifecycleOwner) {
            displayCategories(it)
        }

        viewModel.navigation.observe(viewLifecycleOwner) { navFunction ->
            navFunction?.invoke(this@AddItemFragment)
        }
    }


    private fun displayLocations(locations: List<LocationModel>?) {
        locations ?: return

        dataBinding.locationSpinner.adapter = ArrayAdapter(
            requireContext(),
            android.R.layout.simple_list_item_1,
            locations.map { it.title }
        )

        locations.indexOf(viewModel.selectedLocation.value)
            .takeIf { it > -1 }
            ?.let { dataBinding.locationSpinner.setSelection(it) }
    }

    private fun displayCategories(categories: List<ItemCategory>?) {
        categories ?: return

        dataBinding.categorySpinner.adapter = ArrayAdapter(
            requireContext(),
            android.R.layout.simple_list_item_1,
            categories.map { it.title }
        )

        categories.indexOf(viewModel.selectedCategory.value)
            .takeIf { it > -1 }
            ?.let { dataBinding.categorySpinner.setSelection(it) }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == Constants.ActivityRequestCodes.RC_ITEM_IMAGE_PICK && resultCode == RESULT_OK) {
            val selectedImage: Uri = data?.data ?: return
            val filePathColumn = arrayOf(MediaStore.Images.Media.DATA)
            val cursor: Cursor = context?.contentResolver?.query(
                selectedImage,
                filePathColumn, null, null, null
            ) ?: return
            cursor.moveToFirst()
            val columnIndex: Int = cursor.getColumnIndex(filePathColumn[0])
            val picturePath: String = cursor.getString(columnIndex)
            cursor.close()
            viewModel.onImagePicked(picturePath)
        }
    }

    override fun navigateToItemsList() {
        val action =
            AddItemFragmentDirections.actionAddItemFragmentToNavigationItems()
        findNavController().navigate(action)
    }

    override fun openImagePicker() {
        context?.let {
            activity?.let { _activity ->
                checkStorageRwPermissions(it, _activity) {
                    val intent =
                        Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI)
                    startActivityForResult(
                        intent,
                        Constants.ActivityRequestCodes.RC_ITEM_IMAGE_PICK
                    )
                }
            }
        }
    }
}