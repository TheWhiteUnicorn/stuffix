package com.doberman.it.stuffix.ui.utils.selectableList

import android.view.ActionMode
import android.view.Menu
import android.view.MenuItem
import com.doberman.it.stuffix.R

class SelectionActionModeCallback(
    val viewModel: Actions
) : ActionMode.Callback {
    override fun onActionItemClicked(mode: ActionMode?, item: MenuItem?): Boolean {
        if (item?.itemId == R.id.action_archive) {
            viewModel.archiveSelected()
            mode?.finish()
        }
        return true
    }

    override fun onCreateActionMode(mode: ActionMode?, menu: Menu?): Boolean {
        mode?.menuInflater?.inflate(R.menu.multi_select_menu, menu)
        return true
    }

    override fun onPrepareActionMode(mode: ActionMode?, menu: Menu?): Boolean = false
    override fun onDestroyActionMode(mode: ActionMode?) = viewModel.cancelSelection()

    interface Actions {
        fun archiveSelected()
        fun cancelSelection()
    }
}