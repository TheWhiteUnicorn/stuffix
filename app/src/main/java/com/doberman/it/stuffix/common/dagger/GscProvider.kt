/**
 * (c) 2020 Denis Erashov
 * This code is licensed under MIT license (see LICENSE.txt for details)
 */

package com.doberman.it.stuffix.common.dagger

import android.content.Context
import androidx.lifecycle.MutableLiveData
import com.doberman.it.stuffix.common.data.auth.UserProfile
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignIn.getLastSignedInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInOptions

/**
 * Provides an interface to interact with Google Sign-In Client
 *
 * @property context application context
 */
class GscProvider(val context: Context) {
    val googleSignInClient = GoogleSignIn.getClient(
        context,
        GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().build()
    )

    val currSignedInAccount = MutableLiveData(actualAccount())

    /**
     * Force update local live data property of google account with data from GSC
     */
    fun actualizeSignedInAccount():UserProfileImpl? {
        val value = actualAccount()
        currSignedInAccount.value = value
        return value
    }

    /**
     * Current account from GSC
     */
    private fun actualAccount() =  getLastSignedInAccount(context)?.let(::UserProfileImpl)

    /**
     * А data class that holds data about user profile
     *
     * @property id an id of a user, the same as on the backend
     * @property googleId user id provided by google auth service
     * @property email
     * @property firstName
     * @property lastName
     */
    data class UserProfileImpl(
        override val id: Long? = null,
        override val googleId: String?,
        override val email: String?,
        override val firstName: String?,
        override val lastName: String?
    ) : UserProfile {
        constructor(gsa: GoogleSignInAccount) : this(
            googleId = gsa.id,
            email = gsa.email,
            firstName = gsa.givenName,
            lastName = gsa.familyName
        )
    }
}