package com.doberman.it.stuffix.ui.items.add

import com.doberman.it.stuffix.common.data.itemCategories.ItemCategoriesDao
import com.doberman.it.stuffix.common.data.itemCategories.ItemCategory
import com.doberman.it.stuffix.common.data.items.ItemModel
import com.doberman.it.stuffix.common.data.items.ItemsDao
import com.doberman.it.stuffix.common.data.locations.LocationModel
import com.doberman.it.stuffix.common.data.locations.LocationsDao
import kotlinx.coroutines.flow.Flow

class AddItemRepositoryImpl(
    private val itemsDao: ItemsDao,
    private val locationsDao: LocationsDao,
    private val categoriesDao: ItemCategoriesDao
) : AddItemRepository {
    override fun locationsFlow(): Flow<List<LocationModel>> = locationsDao.locationsFlow()
    override fun categoriesFlow(): Flow<List<ItemCategory>> = categoriesDao.categoriesFlow()

    override suspend fun addTravelItem(item: ItemModel) {
        itemsDao.add(ItemsDao.ItemModelImpl(item))
    }
}
