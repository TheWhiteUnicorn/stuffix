package com.doberman.it.stuffix.ui.travels.list

import com.doberman.it.stuffix.common.data.travels.TravelsDao
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.withContext

class TravelsListRepositoryImpl(
    private val dao: TravelsDao
) : TravelsListRepository {
    override fun getTravelsList(): Flow<List<TravelsListRepository.Travel>> {
        return dao.allWithLocationsFlow()
            .map { list ->
                list.map {
                    TravelsListRepository.Travel(
                        it.travelsModel,
                        it.startPoint,
                        it.destinationPoint
                    )
                }
            }
    }
}
