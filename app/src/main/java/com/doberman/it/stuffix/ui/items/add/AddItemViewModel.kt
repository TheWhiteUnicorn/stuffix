package com.doberman.it.stuffix.ui.items.add

import androidx.lifecycle.*
import com.doberman.it.stuffix.common.archutils.LiveProperty
import com.doberman.it.stuffix.common.archutils.SingleHandledEvent
import com.doberman.it.stuffix.common.data.itemCategories.ItemCategory
import com.doberman.it.stuffix.common.data.items.ItemsDao
import com.doberman.it.stuffix.common.data.locations.LocationModel
import kotlinx.coroutines.launch


class AddItemViewModel(
    private val addItemRepository: AddItemRepository
) : ViewModel() {
    val navigation = SingleHandledEvent<(Navigation) -> Unit>()
    val state = LiveProperty<State>(State.Normal)
    private var _state: State by state

    val locations = addItemRepository
        .locationsFlow()
        .asLiveData(viewModelScope.coroutineContext)

    val categories = addItemRepository
        .categoriesFlow()
        .asLiveData(viewModelScope.coroutineContext)


    val title = MutableLiveData<String>()
    val description = MutableLiveData<String>()

    val imagePath = LiveProperty<String?>(null)
    private var _imagePath: String? by imagePath

    var selectedLocation = LiveProperty<LocationModel?>(null)
    private var _selectedLocation: LocationModel? by selectedLocation

    var selectedCategory = LiveProperty<ItemCategory?>(null)
    private var _selectedCategory: ItemCategory? by selectedCategory

    val isValid = MediatorLiveData<Boolean>().apply {
        fun validate() {
            this.value = title.value?.isNotEmpty() == true
        }

        this.addSource(title) { validate() }
    }

    fun onSelectLocation(position: Int) {
        locations.value?.getOrNull(position)?.let { _selectedLocation = it }
    }

    fun onSelectCategory(position: Int) {
        categories.value?.getOrNull(position)?.let { _selectedCategory = it }
    }

    fun onImagePickClick() {
        navigation.value = { it.openImagePicker() }
    }

    fun onImagePicked(picturePath: String) = picturePath.let {
        _imagePath = it
    }


    fun onAddItem() = viewModelScope.launch {
        _state = State.Adding
        try {
            addItemRepository.addTravelItem(
                ItemsDao.ItemModelImpl(
                    id = 0,
                    title = title.value?.takeIf { it.isNotEmpty() }
                        ?: throw Exception("Missing requited title value"),
                    description = description.value?.takeIf { it.isNotEmpty() },
                    categoryId = _selectedCategory?.id,
                    locationId = _selectedLocation?.id,
                    photo = _imagePath
                )
            )
        } catch (t: Throwable) {
            _state = State.Error(t)
            return@launch
        }
        _state = State.Normal
        navigation.value = { it.navigateToItemsList() }
    }

    interface Navigation {
        fun navigateToItemsList()
        fun openImagePicker()
    }

    sealed class State {
        object Normal : State()
        object Adding : State()
        class Error(val throwable: Throwable) : State()
    }
}
