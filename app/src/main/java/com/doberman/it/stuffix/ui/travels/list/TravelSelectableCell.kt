package com.doberman.it.stuffix.ui.travels.list

import com.doberman.it.stuffix.databinding.ItemTravelBinding
import com.doberman.it.stuffix.ui.utils.recycler.IDataBindingView

class TravelSelectableCell(
    private val viewModel: TravelSubViewModel
): IDataBindingView<ItemTravelBinding> {
    override val layoutID: Int = com.doberman.it.stuffix.R.layout.item_travel

    override fun bindTo(binding: ItemTravelBinding) {
        binding.subViewModel = viewModel
    }

    override fun unbindFrom(binding: ItemTravelBinding) = Unit
}