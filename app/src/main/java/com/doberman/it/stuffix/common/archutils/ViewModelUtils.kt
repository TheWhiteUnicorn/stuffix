package com.doberman.it.stuffix.common.archutils

import androidx.annotation.MainThread
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelLazy
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelStoreOwner
import com.doberman.it.stuffix.common.Application
import com.doberman.it.stuffix.common.dagger.RepositoryComponent

@MainThread
inline fun <reified VM : ViewModel> ViewModelStoreOwner.appViewModels(
    noinline ownerProducer: (() -> ViewModelStoreOwner)? = null,
    noinline constructor: RepositoryComponent.() -> VM
): ViewModelLazy<VM> {
    val producer = object : ViewModelProvider.Factory {
        @Suppress("UNCHECKED_CAST")
        override fun <T : ViewModel?> create(modelClass: Class<T>): T =
            constructor(Application.repositories) as T
    }

    return ViewModelLazy(
        viewModelClass = VM::class,
        storeProducer = { (ownerProducer?.invoke() ?: this).viewModelStore },
        factoryProducer = { producer }
    )
}
