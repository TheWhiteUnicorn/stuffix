package com.doberman.it.stuffix.common.data.locations

interface LocationModel {
    val id: Long
    val title: String
    val description: String
    val address: String
    val archived: Boolean
}