package com.doberman.it.stuffix.ui.itemsSet.make

import com.doberman.it.stuffix.common.data.itemCategories.ItemCategory
import com.doberman.it.stuffix.common.data.items.ItemsDao
import com.doberman.it.stuffix.common.data.locations.LocationModel
import kotlinx.coroutines.flow.Flow

interface ItemsSetRepository {
    fun getItemsFlow(): Flow<List<Item>>
    suspend fun updateItemsSet(selectedItems: Set<ItemsSetViewModel.SelectedItemData>, itemsSetId: Long)

    class Item(
        val header: ItemsDao.ItemModelImpl,
        val location: LocationModel,
        val category: ItemCategory
    )
}