package com.doberman.it.stuffix.common.dagger

import com.doberman.it.stuffix.common.Constants
import com.doberman.it.stuffix.common.data.auth.AuthApiService
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RetrofitProvider {
    private val httpClient: OkHttpClient.Builder

    init {
        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

        httpClient = OkHttpClient.Builder()
        httpClient.addInterceptor(loggingInterceptor)
    }

    private val retrofit = Retrofit.Builder()
        .baseUrl(Constants.API.BASE_URL)
        .addConverterFactory(GsonConverterFactory.create(GsonBuilder().setLenient().create()))
        .client(httpClient.build())
        .build()

    fun auth(): AuthApiService = retrofit.create(AuthApiService::class.java)
}