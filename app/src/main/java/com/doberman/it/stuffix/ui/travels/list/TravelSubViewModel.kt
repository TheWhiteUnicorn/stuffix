package com.doberman.it.stuffix.ui.travels.list

import com.doberman.it.stuffix.common.data.travels.TravelModel

class TravelSubViewModel(
    val model: TravelsListRepository.Travel,
    val parent: TravelsListViewModel
) {
    fun onClick() = parent.onItemPressed(model.header.id)
}