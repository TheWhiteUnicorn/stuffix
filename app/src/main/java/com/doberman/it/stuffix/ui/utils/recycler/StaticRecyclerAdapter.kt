package com.doberman.it.stuffix.ui.utils.recycler

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.OnLifecycleEvent
import androidx.recyclerview.widget.RecyclerView

/**
 * Universal recycler view adapter
 *
 * @param T type of list cell's databinding class
 * @property lifecycleOwner
 * @constructor
 * Subscribes this Recycler View Adapter as an observer of provided lifecycleOwner
 *
 * @param cells a list of cells wrapped up in IDataBindingView
 */
open class StaticRecyclerAdapter<T : ViewDataBinding>(
    cells: List<IDataBindingView<T>>,
    private val lifecycleOwner: LifecycleOwner
) : RecyclerView.Adapter<StaticRecyclerAdapter.ViewHolder<T>>(), LifecycleObserver {

    protected var cells = cells.toMutableList()
    private val visibleCells = mutableMapOf<ViewHolder<T>, IDataBindingView<T>>()

    init {
        lifecycleOwner.lifecycle.addObserver(this)
    }

    /**
     * Update cells list with new values
     *
     * @param newList
     */
    open fun updateList(newList: List<IDataBindingView<T>>) {
        this.cells = newList.toMutableList()
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int = cells.size
    override fun onBindViewHolder(holder: ViewHolder<T>, position: Int) {
        val cell = cells[position]
        cell.bindTo(holder.binding)
        visibleCells[holder] = cell
    }

    override fun onViewRecycled(holder: ViewHolder<T>) {
        super.onViewRecycled(holder)
        val cell = visibleCells.remove(holder)
        if (cell != null) {
            holder.binding.unbind()
            cell.unbindFrom(holder.binding)
        }
    }

    override fun onViewAttachedToWindow(holder: ViewHolder<T>) {
        super.onViewAttachedToWindow(holder)
        holder.binding.lifecycleOwner = lifecycleOwner
    }

    override fun onViewDetachedFromWindow(holder: ViewHolder<T>) {
        super.onViewDetachedFromWindow(holder)
        holder.binding.lifecycleOwner = null
    }

    override fun getItemViewType(position: Int): Int = cells[position].layoutID

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ViewHolder<T> {
        val binding: T =
            DataBindingUtil.inflate(LayoutInflater.from(viewGroup.context), i, viewGroup, false)
        return ViewHolder(binding)
    }

    override fun onDetachedFromRecyclerView(recyclerView: RecyclerView) {
        super.onDetachedFromRecyclerView(recyclerView)
        clearAll()
    }

    var destroyed = false
        private set

    /**
     * Method called when Recycler View will be destroyed
     *
     */
    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    private fun onDestroy() {
        clearAll()
        destroyed = true
        lifecycleOwner.lifecycle.removeObserver(this)
    }

    /**
     * Clear visible cells list
     *
     */
    private fun clearAll() {
        visibleCells.forEach { (holder, binding) ->
            binding.unbindFrom(holder.binding)
            holder.binding.lifecycleOwner = null
        }
        visibleCells.clear()
    }

    /**
     * A static view holder class
     *
     * @param T type of list cell's databinding class
     * @property binding a certaing cell's databinding class instance
     */
    class ViewHolder<T : ViewDataBinding>(val binding: T) : RecyclerView.ViewHolder(binding.root)
}
