package com.doberman.it.stuffix.ui.utils.popups

import android.view.View
import com.doberman.it.stuffix.R

object ExceptionHelper {
    fun showSnack(view: View, throwable: Throwable) {
        PopupGenerator.makeSnack(view, getMessageText(throwable)).show()
    }

    private fun getMessageText(throwable: Throwable): String {
        return throwable.localizedMessage ?: R.string.exc_unknown.toString()
    }
}

