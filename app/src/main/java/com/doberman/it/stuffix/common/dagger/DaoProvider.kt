package com.doberman.it.stuffix.common.dagger

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.doberman.it.stuffix.common.data.RoomDateTypeConverter
import com.doberman.it.stuffix.common.data.RoomUriTypeConverter
import com.doberman.it.stuffix.common.data.itemCategories.ItemCategoriesDao
import com.doberman.it.stuffix.common.data.itemTransactions.ItemTransactionsDao
import com.doberman.it.stuffix.common.data.items.ItemsDao
import com.doberman.it.stuffix.common.data.itemsSet.ItemsSetDao
import com.doberman.it.stuffix.common.data.locations.LocationsDao
import com.doberman.it.stuffix.common.data.travels.TravelsDao

@Database(
    entities = [
        ItemCategoriesDao.ItemCategoriesModel::class,
        ItemsDao.ItemModelImpl::class,
        ItemTransactionsDao.ItemTransactionModelImpl::class,
        LocationsDao.LocationModelImpl::class,
        ItemsSetDao.ItemsSetModelImpl::class,
        ItemsSetDao.ItemInItemsSetModelImpl::class,
        TravelsDao.TravelModelImpl::class
    ],
    version = 12,
    exportSchema = false
)
@TypeConverters(RoomDateTypeConverter::class, RoomUriTypeConverter::class)
abstract class DaoProvider : RoomDatabase() {
    abstract fun locationsDao(): LocationsDao
    abstract fun itemsDao(): ItemsDao
    abstract fun travelsDao(): TravelsDao
    abstract fun itemCategoriesDao(): ItemCategoriesDao
    abstract fun itemsSet(): ItemsSetDao
    abstract fun itemTransactions(): ItemTransactionsDao
}