package com.doberman.it.stuffix.ui.utils.selectableList

object SelectableListItemViewUtil {
    @JvmStatic
    fun isInSelectionMode(selectedIds: Set<Long>?): Boolean = selectedIds != null

    @JvmStatic
    fun checked(modelId: Long, selectedIds: Set<Long>?): Boolean =
        selectedIds != null && modelId in selectedIds
}