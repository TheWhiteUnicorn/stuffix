package com.doberman.it.stuffix.ui.utils.popups

import android.app.Activity
import android.app.AlertDialog
import android.view.View
import com.google.android.material.snackbar.Snackbar

object PopupGenerator {
    fun makeSnack(view: View, text: String): Snackbar = Snackbar.make(
            view,
            text,
            Snackbar.LENGTH_LONG
        )
            .setAction("CLOSE") { }


    fun makeAlert(activity: Activity, title: String, message: String): AlertDialog = AlertDialog.Builder(activity)
        .setTitle(title)
        .setMessage(message)
        .create()
}