package com.doberman.it.stuffix.ui.auth.signUp

interface SignUpRepository {
    suspend fun register(email: String, password: String, givenName: String, familyName: String)
}