package com.doberman.it.stuffix.ui.travels.view

import android.content.Context
import android.graphics.drawable.Drawable
import com.doberman.it.stuffix.R
import com.doberman.it.stuffix.common.data.travels.TravelModel

object ViewTravelViewUtil {
    @JvmStatic
    fun getStatusIcon(status: TravelModel.TravelStatus?, context: Context): Drawable? {
        status?.let {
            return when (it) {
                TravelModel.TravelStatus.PLANNED -> context.getDrawable(R.drawable.creation)
                TravelModel.TravelStatus.ACTIVE -> context.getDrawable(R.drawable.airballoon)
                TravelModel.TravelStatus.FINISHED -> context.getDrawable(R.drawable.flag_checkered)
                TravelModel.TravelStatus.CANCELED -> context.getDrawable(R.drawable.cancel)
            }
        } ?: return context.getDrawable(R.drawable.creation) // Can this crutch be fixed?
    }

    @JvmStatic
    fun getStatusIconColor(status: TravelModel.TravelStatus?, context: Context): Int? {
        status?.let {
            return when (it) {
                TravelModel.TravelStatus.PLANNED -> context.getColor(R.color.colorYellow)
                TravelModel.TravelStatus.ACTIVE -> context.getColor(R.color.colorGreen)
                TravelModel.TravelStatus.FINISHED -> context.getColor(R.color.colorDark)
                TravelModel.TravelStatus.CANCELED -> context.getColor(R.color.colorAccent)
            }
        } ?: return context.getColor(R.color.colorYellow)
    }

    @JvmStatic
    fun getStatusText(status: TravelModel.TravelStatus?, context: Context): String? {
        status?.let {
            return when (it) {
                TravelModel.TravelStatus.PLANNED -> context.getString(R.string.travel_status_planned)
                TravelModel.TravelStatus.ACTIVE -> context.getString(R.string.travel_status_active)
                TravelModel.TravelStatus.FINISHED -> context.getString(R.string.travel_status_finished)
                TravelModel.TravelStatus.CANCELED -> context.getString(R.string.travel_status_canceled)
            }
        } ?: return null
    }

    @JvmStatic
    fun getStuffSetSectionHeaderText(status: TravelModel.TravelStatus?, context: Context): String? {
        status?.let {
            return when (it) {
                TravelModel.TravelStatus.PLANNED -> context.getString(R.string.items_set_section_header_planned)
                TravelModel.TravelStatus.ACTIVE -> context.getString(R.string.items_set_section_header_active)
                TravelModel.TravelStatus.FINISHED -> context.getString(R.string.items_set_section_header_finished)
                TravelModel.TravelStatus.CANCELED -> context.getString(R.string.items_set_section_header_canceled)
            }
        } ?: return null
    }

    @JvmStatic
    fun getFabText(status: TravelModel.TravelStatus?, context: Context): String? {
        status?.let {
            return when (it) {
                TravelModel.TravelStatus.PLANNED -> context.getString(R.string.fab_label_dispatch)
                TravelModel.TravelStatus.ACTIVE -> context.getString(R.string.fab_label_finish)
                else -> null
            }
        } ?: return null
    }

    @JvmStatic
    fun getFabIcon(status: TravelModel.TravelStatus?, context: Context): Drawable? {
        status?.let {
            return when (it) {
                TravelModel.TravelStatus.PLANNED -> context.getDrawable(R.drawable.airballoon)
                TravelModel.TravelStatus.ACTIVE -> context.getDrawable(R.drawable.flag_checkered)
                else -> context.getDrawable(R.drawable.airballoon)
            }
        } ?: return context.getDrawable(R.drawable.airballoon)
    }

    @JvmStatic
    fun isFabVisible(status: TravelModel.TravelStatus?): Boolean {
        status?.let {
            return when (it) {
                TravelModel.TravelStatus.PLANNED -> true
                TravelModel.TravelStatus.ACTIVE -> true
                else -> false
            }
        } ?: return false
    }
}