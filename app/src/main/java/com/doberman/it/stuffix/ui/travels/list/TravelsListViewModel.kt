package com.doberman.it.stuffix.ui.travels.list

import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.doberman.it.stuffix.common.archutils.LiveProperty
import com.doberman.it.stuffix.common.archutils.SingleHandledEvent
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch


class TravelsListViewModel(
    repository: TravelsListRepository
) : ViewModel() {

    val navigation = SingleHandledEvent<(Navigation) -> Unit>()

    val travels = repository
        .getTravelsList()
        .map { list -> list.map {TravelSubViewModel(it, this)} }
        .asLiveData(viewModelScope.coroutineContext)

    fun onFabPress() = viewModelScope.launch {
        navigation.value = { it.navigateToAddTravel() }
    }

    fun onItemPressed(id: Long) {
        navigation.value = {it.navigateToViewTravel(id)}
    }

    interface Navigation {
        fun navigateToAddTravel()
        fun navigateToViewTravel(travelId: Long)
    }
}
