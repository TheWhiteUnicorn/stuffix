package com.doberman.it.stuffix.ui.locations.view

import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData

class ViewLocationViewModel(
    viewLocationRepository: ViewLocationRepository,
    locationId: Long
) : ViewModel() {
    val location = viewLocationRepository.getLocation(locationId).asLiveData()
}