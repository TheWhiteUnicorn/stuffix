package com.doberman.it.stuffix.ui.items.add

object AddItemViewUtil {
    @JvmStatic
    fun submissionAvailable(isFormValid: Boolean, state: AddItemViewModel.State?): Boolean {
        return isFormValid && state != AddItemViewModel.State.Adding
    }
}