package com.doberman.it.stuffix.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.asLiveData
import com.doberman.it.stuffix.common.dagger.GscProvider
import com.doberman.it.stuffix.common.dagger.SessionManager
import com.doberman.it.stuffix.common.data.auth.UserProfile
import kotlinx.coroutines.ExperimentalCoroutinesApi

class MainActivityRepositoryImpl(
    private val sessionManager: SessionManager,
    private val gsc: GscProvider
) : MainActivityRepository {
    @ExperimentalCoroutinesApi
    private val currEmailPasswordAccount = sessionManager.userProfileFlow().asLiveData()
    private val currGoogleAccount = gsc.currSignedInAccount

    @ExperimentalCoroutinesApi
    override fun getUserProfile(): LiveData<UserProfile> {
        val mediatorLiveData = MediatorLiveData<UserProfile>()
        mediatorLiveData.addSource(currEmailPasswordAccount) { value ->
            if (value?.id != null || currGoogleAccount.value?.googleId == null)
                mediatorLiveData.value = value
        }

        mediatorLiveData.addSource(currGoogleAccount) { value ->
            if (value?.googleId != null || currEmailPasswordAccount.value?.id == null)
                mediatorLiveData.value = value
        }
        return mediatorLiveData
    }

    override fun suspendAuthenticatedSession() {
        gsc.googleSignInClient.signOut()
        sessionManager.suspendAuthenticatedSession()
        gsc.actualizeSignedInAccount()
    }

    override fun actualizeSignedInAccount() {
        gsc.actualizeSignedInAccount()
    }
}