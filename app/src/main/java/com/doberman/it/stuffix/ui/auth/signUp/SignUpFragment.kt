package com.doberman.it.stuffix.ui.auth.signUp

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.observe
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.doberman.it.stuffix.R
import com.doberman.it.stuffix.RootNavigationGraphDirections
import com.doberman.it.stuffix.common.archutils.appViewModels
import com.doberman.it.stuffix.databinding.FragmentSignUpBinding

class SignUpFragment : Fragment(), SignUpViewModel.Navigation {
    private val viewModel: SignUpViewModel by appViewModels {
        SignUpViewModel(signUp())
    }
    private lateinit var dataBinding: FragmentSignUpBinding
    private val rootNavController: NavController? by lazy { activity?.findNavController(R.id.nav_host_fragment) }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dataBinding = FragmentSignUpBinding.inflate(inflater, container, false)
        return dataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        dataBinding.viewModel = viewModel
        dataBinding.lifecycleOwner = viewLifecycleOwner


        viewModel.navigation.observe(viewLifecycleOwner) { navFunction ->
            navFunction?.invoke(this@SignUpFragment)
        }
    }

    override fun navigateToSignIn() {
        val action =
            SignUpFragmentDirections.actionNavigationFragmentSignUpToNavigationFragmentSignIn()
        findNavController().navigate(action)
    }

    override fun navigateToHomeScreen() {
        val action = RootNavigationGraphDirections.actionGlobalNavigationFragmentHomeScreen()
        findNavController().navigate(action)
    }
}