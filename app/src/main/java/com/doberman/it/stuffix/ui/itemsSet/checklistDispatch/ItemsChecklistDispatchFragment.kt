package com.doberman.it.stuffix.ui.itemsSet.checklistDispatch

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.observe
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.doberman.it.stuffix.common.archutils.appViewModels
import com.doberman.it.stuffix.databinding.FragmentItemsChecklistDispatchBinding
import com.doberman.it.stuffix.databinding.ItemSetItemForChecklistBinding
import com.doberman.it.stuffix.ui.utils.recycler.StaticRecyclerAdapter

class ItemsChecklistDispatchFragment : Fragment(), ItemsCheckLIstDispatchViewModel.Navigation {
    private val args: ItemsChecklistDispatchFragmentArgs by navArgs()
    private val viewModel: ItemsCheckLIstDispatchViewModel by appViewModels {
        ItemsCheckLIstDispatchViewModel(itemsChecklistDispatch(), args.travelId)
    }

    private lateinit var dataBinding: FragmentItemsChecklistDispatchBinding
    private lateinit var recyclerAdapter: StaticRecyclerAdapter<ItemSetItemForChecklistBinding>

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dataBinding = FragmentItemsChecklistDispatchBinding.inflate(inflater, container, false)
        return dataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dataBinding.lifecycleOwner = viewLifecycleOwner
        dataBinding.viewModel = viewModel

        recyclerAdapter = StaticRecyclerAdapter(emptyList(), viewLifecycleOwner)
        dataBinding.itemsChecklistDispatchRecyclerView.adapter = recyclerAdapter

        viewModel.itemsToTake.observe(viewLifecycleOwner, Observer {
            it?.let { recyclerAdapter.updateList(it.map(::ItemSelectableCell)) }
        })

        viewModel.navigation.observe(viewLifecycleOwner) { it?.invoke(this) }
    }

    override fun navigateToViewTravel() {
        findNavController().navigateUp()
    }
}