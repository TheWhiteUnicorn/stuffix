package com.doberman.it.stuffix.ui.travels.list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.observe
import androidx.navigation.fragment.findNavController
import com.doberman.it.stuffix.common.archutils.appViewModels
import com.doberman.it.stuffix.databinding.FragmentTravelsListBinding
import com.doberman.it.stuffix.databinding.ItemLocationBinding
import com.doberman.it.stuffix.databinding.ItemTravelBinding
import com.doberman.it.stuffix.ui.items.list.ItemsListFragmentDirections
import com.doberman.it.stuffix.ui.utils.popups.ExceptionHelper
import com.doberman.it.stuffix.ui.utils.recycler.StaticRecyclerAdapter

class TravelsListFragment : Fragment(), TravelsListViewModel.Navigation {

    private val viewModel: TravelsListViewModel by appViewModels {
        TravelsListViewModel(travels())
    }

    private lateinit var dataBinding: FragmentTravelsListBinding
    private lateinit var adapter: StaticRecyclerAdapter<ItemTravelBinding>

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dataBinding = FragmentTravelsListBinding.inflate(inflater, container, false)
        return dataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        dataBinding.lifecycleOwner = viewLifecycleOwner
        dataBinding.viewModel = viewModel

        adapter = StaticRecyclerAdapter(emptyList(), viewLifecycleOwner)
        dataBinding.travelsListRecyclerView.adapter = adapter

        viewModel.navigation.observe(viewLifecycleOwner) { it?.invoke(this) }

        viewModel.travels.observe(viewLifecycleOwner) {
            adapter.updateList(it.map(::TravelSelectableCell))
        }
    }

    override fun navigateToAddTravel() {
        val action =
            TravelsListFragmentDirections.actionNavigationFragmentTravelsToAddTravelFragment()
        findNavController().navigate(action)
    }

    override fun navigateToViewTravel(travelId: Long) {
        val action =
            TravelsListFragmentDirections.actionTravelsToViewTravel(travelId)
        findNavController().navigate(action)
    }

}
