package com.doberman.it.stuffix.common.data

import android.net.Uri
import androidx.room.TypeConverter
import java.util.*

class RoomDateTypeConverter {
    @TypeConverter
    fun fromTimestamp(value: Long?): Date? = value?.let(::Date)

    @TypeConverter
    fun dateToTimestamp(date: Date?): Long? = date?.time
}

class RoomUriTypeConverter {
    @TypeConverter
    fun uriToString(uri: Uri?): String? = uri.toString()

    @TypeConverter
    fun stringToUri(string: String?): Uri? = Uri.parse(string)
}
