/**
 * (c) 2020 Denis Erashov
 * This code is licensed under MIT license (see LICENSE.txt for details)
 */

package com.doberman.it.stuffix.common.archutils

import androidx.lifecycle.LiveData
import kotlin.reflect.KProperty

/**
 * Arch util that can be used to connect plain internal viewModel variable to corresponding
 * LiveData properties
 *
 * @param T
 * @property _value
 */
class LiveProperty<T>(private var _value: T) : LiveData<T>(_value) {
    operator fun getValue(thisRef: Any?, property: KProperty<*>): T = _value

    operator fun setValue(thisRef: Any?, property: KProperty<*>, value: T) {
        _value = value
        postValue(_value)
    }
}