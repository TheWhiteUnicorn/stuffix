package com.doberman.it.stuffix.ui

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.observe
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import com.doberman.it.stuffix.R
import com.doberman.it.stuffix.RootNavigationGraphDirections
import com.doberman.it.stuffix.common.archutils.appViewModels
import com.doberman.it.stuffix.databinding.ActivityMainBinding
import com.doberman.it.stuffix.databinding.DrawerNavigationHeaderBinding
import com.google.android.material.navigation.NavigationView

const val ARG_START_AUTHENTICATED = "com.doberman.it.stuffix.ARG_START_AUTHENTICATED"

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {
    private val viewModel: MainActivityViewModel by appViewModels {
        MainActivityViewModel(mainActivity())
    }
    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var binding: ActivityMainBinding
    private lateinit var navHeaderBinding: DrawerNavigationHeaderBinding
    private lateinit var rootNavController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        navHeaderBinding =
            DrawerNavigationHeaderBinding.inflate(layoutInflater, binding.drawerNavView, false)
        binding.drawerNavView.addHeaderView(navHeaderBinding.root)

        rootNavController = findNavController(R.id.nav_host_fragment)
        val graph = rootNavController.navInflater.inflate(R.navigation.root_navigation)
        graph.startDestination = if (
            intent.getBooleanExtra(ARG_START_AUTHENTICATED, false)
        ) {
            R.id.navigation_fragment_travels
        } else {
            R.id.auth_navigation_graph
        }
        rootNavController.graph = graph

        viewModel.userProfile.observe(this) { userProfile ->
            navHeaderBinding.userProfile = userProfile
        }
    }

    override fun onStart() {
        super.onStart()
        setupNavigation()
        viewModel.actualizeSignedInAccount()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
    }

    private fun setupNavigation() {
        val toolbar = binding.toolbar
        val bottomNav = binding.bottomNav
        val drawer = binding.drawerLayout

        appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.navigation_fragment_travels,
                R.id.navigation_fragment_items,
                R.id.navigation_fragment_locations
            ), drawer
        )

        toolbar.setupWithNavController(rootNavController, appBarConfiguration)
        bottomNav.setupWithNavController(rootNavController)

        rootNavController.addOnDestinationChangedListener { _, dest, _ ->
            if (dest.id == R.id.navigation_fragment_sign_in
                || dest.id == R.id.navigation_fragment_sign_up
                || dest.id == R.id.navigation_fragment_add_item
                || dest.id == R.id.navigation_fragment_add_location
                || dest.id == R.id.navigation_fragment_add_travel
                || dest.id == R.id.navigation_fragment_view_location
                || dest.id == R.id.navigation_fragment_view_travel
                || dest.id == R.id.navigation_fragment_items_set
                || dest.id == R.id.navigation_fragment_items_checklist_dispatch
            ) {
                bottomNav.visibility = View.GONE
            } else {
                bottomNav.visibility = View.VISIBLE
            }

            if (dest.id == R.id.navigation_fragment_sign_in
                || dest.id == R.id.navigation_fragment_sign_up
            ) {
                toolbar.visibility = View.GONE
            } else {
                toolbar.visibility = View.VISIBLE
            }
        }

        binding.drawerNavView.setNavigationItemSelectedListener(this)
    }

    override fun onNavigationItemSelected(p0: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        val id: Int = p0.itemId

        if (id == R.id.drawer_item_logout) {
            viewModel.suspendAuthenticatedSession()
            val action = RootNavigationGraphDirections.actionGlobalLogOut()
            rootNavController.navigate(action)
        } else if (id == R.id.drawer_item_settings) {
            TODO()
        }

        val drawer = binding.drawerLayout
        drawer.closeDrawer(GravityCompat.START)
        return true
    }
}
